<?php
/**
 * Created by PhpStorm.
 * User: Rem
 * Date: 22.08.2016
 * Time: 16:58
 */

namespace app\data;

use Yii;
use yii\db\Query as BaseQuery;
use yii\db\Connection;

class Query extends BaseQuery
{
    /**
     * @var \yii\db\Connection
     */
    private $_db;

    private $_sql;

    /**
     * Query constructor.
     * @param \yii\db\Connection $db
     * @param array $config
     */
    public function __construct(Connection $db, array $config = [])
    {
        parent::__construct($config);
        $this->_db = $db;
        Yii::$app->set('db', $db);
    }

    public function asString()
    {
        if ($this->_sql !== null) {
            return $this->_sql;
        }
        return $this->createCommand($this->_db)->getRawSql();
    }

    /**
     * @param string $table
     * @param array $data
     * @param array $columns
     * @param array $values
     * @return Query
     */
    public function createInsertQuery($table, $data, $columns = null, $values = null)
    {
        if (empty($data)) {
            return $this;
        }

        if (empty($columns)) {
            $columns = array_keys(current($data));
        }

        if (empty($values)) {
            $values = $columns;
        }

        $columns = '"' . implode('","', $columns) . '"';
        $insertValuesString = '';
        foreach ($data as $item) {
            $insertItem = '';
            foreach ($values as $name) {
                if (array_key_exists($name, $item)) {
                    $value = $item[$name];
                    if ($value === null) {
                        $insertItem .= 'NULL,';
                    } elseif ($value === false) {
                        $insertItem .= "'f',";
                    } elseif ($value === true) {
                        $insertItem .= "'t',";
                    } else {
                        $value = str_replace('\'', '`', $value);
                        $insertItem .= "'{$value}',";
                    }
                }
            }
            if ($insertItem === '') {
                continue;
            }
            $insertValuesString .= '(' . substr($insertItem, 0, -1) . '),';
        }
        if ($insertValuesString === '') {
            echo "No insert data given\n";
            die;
        }

        $insertValuesString = substr($insertValuesString, 0, -1);

        $this->_sql = "INSERT INTO \"{$table}\" ({$columns}) VALUES {$insertValuesString}";

        return $this;
    }

    public function execute()
    {
        return $this->_db->createCommand($this->_sql)->execute();
    }
}