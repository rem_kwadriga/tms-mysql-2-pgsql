<?php
/**
 * Created by PhpStorm.
 * User: Rem
 * Date: 03.11.2016
 * Time: 13:08
 */

namespace app\commands;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\console\Controller;
use app\data\Query;
use yii\db\Transaction;
use yii\db\Connection;

class ImportController extends Controller
{
    const MYSQL_COUNTRY = 'tbl_country';
    const MYSQL_REGION = 'tbl_region';
    const MYSQL_CITY = 'tbl_city';
    const MYSQL_ADDRESS = 'tbl_location';
    const MYSQL_AREA = 'tbl_location';
    const MYSQL_COMPANY = 'tbl_role';
    const MYSQL_USER = 'tbl_user';
    const MYSQL_USER_COMPANY = 'tbl_user_role';
    const MYSQL_EVENT = 'tbl_event';
    const MYSQL_EVENT_TIMING = 'tbl_timing';
    const MYSQL_SCHEME = 'tbl_scheme';
    const MYSQL_MEDIA = 'tbl_multimedia';
    const MYSQL_PLACE = 'tbl_place';
    const MYSQL_ORDER = 'tbl_order';
    const MYSQL_QUOTA = 'tbl_quote_info';
    const MYSQL_TICKET = 'tbl_ticket';
    const MYSQL_ZONE = 'tbl_sector';
    const MYSQL_TIMING = 'tbl_timing';
    const MYSQL_DELIVERY = 'tbl_delivery';
    const MYSQL_TICKET_STORY = 'tbl_history';
    const MYSQL_USER_ROLE = 'tbl_user_role';
    const MYSQL_ROLE = 'tbl_role';

    const PGSQL_COUNTRY = 'geo_country';
    const PGSQL_GEO_AREA = 'geo_area';
    const PGSQL_REGION = 'geo_region';
    const PGSQL_CITY = 'geo_city';
    const PGSQL_ADDRESS = 'geo_address';
    const PGSQL_AREA = 'area';
    const PGSQL_HALL = 'area_file';
    const PGSQL_ZONE = 'area_zone';
    const PGSQL_PLACE = 'area_place';
    const PGSQL_COMPANY = 'company';
    const PGSQL_USER = 'user';
    const PGSQL_USER_POSITION = 'user_position';
    const PGSQL_CLIENT = 'client';
    const PGSQL_CASH = 'cash';
    const PGSQL_EVENT = 'event';
    const PGSQL_MEDIA = 'media';
    const PGSQL_TICKET = 'ticket';
    const PGSQL_ORDER = 'order';
    const PGSQL_QUOTA = 'quota';
    const PGSQL_PAYMENT = 'payment';
    const PGSQL_DELIVERY = 'delivery';
    const PGSQL_QUOTA_TICKET = 'quota_ticket';
    const PGSQL_TICKET_STORY = 'story_ticket';
    const PGSQL_IMPORT_ERROR_LOG = 'import_error_log';

    const EVENT_START_DATE = '20170201';
    const KASA_IN_UA_ID = 11;

    //status=0 - Скасовано
    //status=1 - активно
    //status=2 - продано у квоті (продано К/А)
    //status=3 - повернено з квоти
    //status=5 - тоже активный (похоже для елетронных билетов)
    const TICKET_STATUS_CANCELLED = 0;
    const TICKET_STATUS_ACTIVE = 1;
    const TICKET_STATUS_SOLD_IN_QUOTA = 2;
    const TICKET_STATUS_RETURNED_FROM_QUOTA = 3;
    const TICKET_STATUS_E_ACTIVE = 5;

    const TICKET_TYPE_TICKET = 1;
    const TICKET_TYPE_IN_QUOTA = 2;

    //0- не оплачено
    //1- оплачено
    //2 - приглашение
    const PAYMENT_STATUS_NOT_PAYED = 0;
    const PAYMENT_STATUS_PAID = 1;
    const PAYMENT_STATUS_INVITE = 2;

    const PAYMENT_TYPE_CASH_PICKUP = 1;
    const PAYMENT_TYPE_CARD_PICKUP = 2;
    const PAYMENT_TYPE_CASH_PICKUP_2 = 3;
    const PAYMENT_TYPE_CARD_POST_SERVICE = 4;
    const PAYMENT_TYPE_CASH_POST_SERVICE = 5;
    const PAYMENT_TYPE_CARD_PICKUP_2 = 6;
    const PAYMENT_TYPE_CARD_E_TICKET = 8;

    //0 - кота не передана.
    //1 - кота передана.
    const QUOTA_STATUS_ALLOCATED = 0;
    const QUOTA_STATUS_NOT_ALLOCATED = 0;

    //0 - закрытая
    //1 -электронная квота
    //2 -физическая квота
    //3 -Накладная возврат организатора
    const QUOTA_TYPE_CLOSED = 0; // WAF?!
    const QUOTA_TYPE_E_QUOTA = 1;
    const QUOTA_TYPE_STANDARD = 2;
    const QUOTA_TYPE_INVOICE = 3;

    const TICKET_BLANK_TYPE_STANDARD = 0;
    const TICKET_BLANK_TYPE_A4 = 1;
    const TICKET_BLANK_TYPE_NONE = 6;
    const TICKET_BLANK_TYPE_A5 = 2;

    const DELIVERY_STATUS_SENT = 1;
    const DELIVERY_STATUS_NOT_SENT = 0;
    const DELIVERY_STATUS_RECEIVED = 2;
    const DELIVERY_STATUS_RETURNED = 3;
    const DELIVERY_STATUS_SENT_ON_EMAIL = 5;

    const DELIVERY_TYPE_PICKUP = 1;
    const DELIVERY_TYPE_PICKUP_2 = 2;
    const DELIVERY_TYPE_POST_SERVICE = 3;
    const DELIVERY_TYPE_POST_SERVICE_2 = 4;
    const DELIVERY_TYPE_COURIER = 5;
    const DELIVERY_TYPE_COURIER_2 = 6;
    const DELIVERY_TYPE_EMAIL = 8;

    /**
     * @var Transaction
     */
    private $transaction;

    private $_adminID;

    private $_systemCompanyID;

    //private $eventsIDs = [1874,1823,18,1901,1953,1881,1956,1910,1739,1911,1718,1728,1811,1878,1720,1726,1930,1920,1790,1893];
    //private  $eventsIDs = [18,954,1588,1701,1718,1720,1726,1727,1730,1822,1874,1878,1893,1930,1956,1972,1976,1980,1982,1983,1993,1995,1996,1997,2000,2002,2003,2004,2007,2015,2016,2019,2021,2022,2023,2024,2028,2032,2033,2035,2038];
    private $eventsIDs;
    //private $eventsIDs = [1720];


    private $newAreasIDs = [329];

    private $newHallsIDs = [522];

    private $usersIDs = null;

    private $_checkedIDs = [];

    private $_zonesInfo = [];

    /**
     * Action yii import/all
     */
    public function actionAll()
    {
        $transaction = Yii::$app->pgSql->beginTransaction();
        echo "Total data import start\n";

        $this->actionEvent();
        $this->actionMedia();
        $this->actionOrder();
        $this->actionQuota();
        //$this->actionReadZonesInfo();
        //$this->actionTicket();
        /*$this->actionQuotaTicket();
        $this->actionTicketStory();*/


        /*$this->actionUser();
        $this->actionGeo();
        $this->actionCities();
        $this->actionCompany();
        $this->actionUser();
        $this->actionClient();
        $this->actionCash();
        $this->actionArea();
        $this->actionEvent();
        $this->actionMedia();
        $this->actionOrder();
        $this->actionQuota();
        $this->actionTicket();
        $this->actionTicketStory();*/

        $transaction->commit();
        echo "Total data import end\n";
    }


    // Import data functions

    /**
     * Action yii import/cities
     */
    public function actionCities()
    {
        // Get DB connections instances
        $mySqlDb = Yii::$app->mySql;
        $pgDb = Yii::$app->pgSql;

        // Get PG sql citiesIDs
        $existingCities = (new Query($pgDb))
            ->select('id')
            ->from(self::PGSQL_CITY)
            ->indexBy('id')
            ->all();
        $existingCitiesIDs = array_keys($existingCities);

        // Get events cities IDs
        $eventsCitiesQuery = (new Query($mySqlDb))
            ->select(['id' => 'DISTINCT(area.city_id)'])
            ->from(['event' => self::MYSQL_EVENT])
            ->leftJoin(['scheme' => self::MYSQL_SCHEME], 'scheme.id = event.scheme_id')
            ->leftJoin(['area' => self::MYSQL_AREA], 'area.id = scheme.location_id')
            ->indexBy('id');
        if (($eventIDs = $this->getEventsIds()) !== null) {
            $eventsCitiesQuery->andWhere(['event.id' => $eventIDs]);
        }
        $eventsCitiesIDs = array_keys($eventsCitiesQuery->all());

        // Find not existing cities IDs
        $notExistingCitiesIDs = array_diff($eventsCitiesIDs, $existingCitiesIDs);
        if (empty($notExistingCitiesIDs)) {
            echo "Target DB has all events cities";
            return;
        }

        echo "Import cities data start\n";

        // Create events cities query
        $citiesQuery = (new Query($mySqlDb))
            ->select([
                'id',
                'region_id',
                'area_id' => '(1)',
                'name',
                'is_active' => "('t')",
            ])
            ->from(self::MYSQL_CITY)
            ->where(['id' => $notExistingCitiesIDs]);

        // Get cities list
        if (empty($cities = $citiesQuery->all())) {
            $this->rollback($citiesQuery, "Cities not found");
        }

        /** Write data */

        // Begin transaction
        $this->start($pgDb, 'Transfer cities data started');

        // Create insert users query
        $insertCitiesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_CITY, $cities);
        // Try to insert users
        if (!$insertCitiesQuery->execute()) {
            $this->rollback($insertCitiesQuery, "Can no insert the cities");
        }

        // Set set sequence value - max ID from table "event"
        $this->setSequenceValue(self::PGSQL_CITY);

        $this->commit('Transfer cities data is done');
    }

    /**
     * Action yii import/new-areas
     */
    public function actionNewAreas()
    {
        // Get DB connections instances
        $dataDb = Yii::$app->mySql;
        $pgDb = Yii::$app->pgSql;

        echo "Import New areas data start\n";

        // Create new Areas query
        $newAreasQuery = (new Query($dataDb))
            ->select([
                'id',
                'name',
                'cityID' => 'city_id',
                'address',
            ])
            ->from(self::MYSQL_AREA)
            ->where(['id' => $this->newAreasIDs]);
        // Try to find the new areas
        if (empty($newAreas = $newAreasQuery->all())) {
            $this->rollback($newAreasQuery, 'New Areas not found');
        }

        // Filter new areas: if area exist - skip it
        foreach ($newAreas as $index => $newArea) {
            $countQuery = (new Query($pgDb))
                ->from(self::PGSQL_AREA)
                ->where(['id' => $newArea['id']]);
            if ($countQuery->count() > 0) {
                unset($newAreas[$index]);
            }
        }

        //Begin the transaction
        $this->start($pgDb);

        // Insert new areas addresses
        foreach ($newAreas as $index => $area) {
            // Try to find the address
            $address = (new Query($pgDb))
                ->select('id')
                ->from(self::PGSQL_ADDRESS)
                ->where([
                    'city_id' => $area['cityID'],
                    'address' => $area['address'],
                ])
                ->one();
            if (empty($address)) {
                // If address is not found - create new
                $newAddressData = ['city_id' => $area['cityID'], 'address' => $area['address']];
                if (!$pgDb->createCommand()->insert(self::PGSQL_ADDRESS, $newAddressData)->execute()) {
                    $this->rollback(new Query($pgDb), 'Can not insert the address ' . $area['address']);
                }
                $addressID = $this->getLastInsertID(self::PGSQL_ADDRESS);
            } else {
                $addressID = $address['id'];
            }

            unset($area['cityID'], $area['address']);
            $area['address_id'] = $addressID;

            $newAreas[$index] = $area;
        }

        if (!empty($newAreas)) {
            // Create insert areas query
            $insertAreasQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_AREA, $newAreas);
            // Try to insert areas
            if (!$insertAreasQuery->execute()) {
                $this->rollback($insertAreasQuery, "Can no insert the areas");
            }
            $this->setSequenceValue(self::PGSQL_AREA);
        }

        // Create new halls query
        $newHallsQuery = (new Query($dataDb))
            ->select([
                'id',
                'area_id' => 'location_id',
                'name',
                'file_path' => "CONCAT('scheme/', id, '/svg.svg')",
            ])
            ->from(self::MYSQL_SCHEME)
            ->where(['location_id' => $this->newAreasIDs]);
        // Try to find new halls
        if (empty($newHalls = $newHallsQuery->all())) {
            $this->rollback($newHallsQuery, 'New halls not found');
        }

        // Filter new halls: if hall exist - skip it
        foreach ($newHalls as $index => $newHall) {
            $countQuery = (new Query($pgDb))
                ->from(self::PGSQL_HALL)
                ->where(['id' => $newHall['id']]);
            if ($countQuery->count() > 0) {
                unset($newHalls[$index]);
            }
        }

        if (empty($newHalls)) {
            $this->commit("New areas not found");
        }

        // Create insert halls query
        $insertHallsQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_HALL, $newHalls);
        // Try to insert halls
        if (!$insertHallsQuery->execute()) {
            $this->rollback($insertHallsQuery, 'Can not insert halls');
        }
        $this->setSequenceValue(self::PGSQL_HALL);

        // Get halls IDs
        $count = count($newHalls);

        // Commit transaction
        $this->commit("Imported {$count} schemes");
    }

    /**
     * Action yii import/new-halls
     */
    public function actionNewHalls()
    {
        // Get DB connections instances
        $dataDb = Yii::$app->mySql;
        $pgDb = Yii::$app->pgSql;

        if (empty($this->newHallsIDs)) {
            $existingSchemes = (new Query($pgDb))
                ->select('id')
                ->from(self::PGSQL_HALL)
                ->indexBy('id')
                ->all();
            $existingSchemesIDs = array_keys($existingSchemes);

            $eventSchemes = (new Query($dataDb))
                ->select(['id' => 'DISTINCT(scheme_id)'])
                ->from(self::MYSQL_EVENT)
                ->indexBy('id')
                ->all();
            $eventSchemesIDs = array_keys($eventSchemes);

            $notExistingSchemesIDs = array_diff($eventSchemesIDs, $existingSchemesIDs);
        } else {
            $notExistingSchemesIDs = $this->newHallsIDs;
        }

        if (empty($notExistingSchemesIDs)) {
            echo "Target DB has all events schemes";
            return;
        }

        echo "Import New halls data start\n";

        // Create new halls query
        $newHallsQuery = (new Query($dataDb))
            ->select([
                'id',
                'area_id' => 'location_id',
                'name',
                'file_path' => "CONCAT('scheme/', id, '/svg.svg')",
            ])
            ->from(self::MYSQL_SCHEME)
            ->where(['id' => $notExistingSchemesIDs]);
        // Try to find new halls
        if (empty($newHalls = $newHallsQuery->all())) {
            $this->rollback($newHallsQuery, 'New halls not found');
        }

        //Begin the transaction
        $this->start($pgDb);

        // Drop halls from PG-database
        $pgDb->createCommand()->delete(self::PGSQL_HALL, ['id' => $this->newHallsIDs])->execute();

        // Create insert halls query
        $insertHallsQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_HALL, $newHalls);
        // Try to insert halls
        if (!$insertHallsQuery->execute()) {
            $this->rollback($insertHallsQuery, 'Can not insert halls');
        }
        $this->setSequenceValue(self::PGSQL_HALL);

        // Get halls IDs
        $count = count($newHalls);

        // Commit transaction
        $this->commit("Imported {$count} schemes");
    }

    /**
     * Action yii import/geo
     */
    public function actionGeo()
    {
        // Get DB connections instances
        $dataDb = Yii::$app->dataSql;
        $pgDb = Yii::$app->pgSql;

        echo "Import Geo data start\n";

        /** Get data */

        // Create select countries query
        $countriesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_COUNTRY);

        // Get countries list
        if (empty($countries = $countriesQuery->all())) {
            $this->rollback($countriesQuery, "Countries not found");
        }

        // Create select areas query
        $areasQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_GEO_AREA);

        // Get areas list
        if (empty($areas = $areasQuery->all())) {
            $this->rollback($areasQuery, "Areas not found");
        }

        // Create select regions query
        $regionsQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_REGION);

        // Get regions list
        if (empty($regions = $regionsQuery->all())) {
            $this->rollback($regionsQuery, "Regions not found");
        }

        // Create select cities query
        $citiesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_CITY);

        // Get cities list
        if (empty($cities = $citiesQuery->all())) {
            $this->rollback($citiesQuery, "Cities not found");
        }

        // Create select addresses query
        $addressesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_ADDRESS);

        // Get addresses list
        if (empty($addresses = $addressesQuery->all())) {
            $this->rollback($addressesQuery, "Addresses not found");
        }


        /** Write data */
        // Begin transaction
        $this->start($pgDb, 'Import countries start');

        // Create insert countries query
        $insertCountriesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_COUNTRY, $countries);
        // Try to insert countries
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the countries");
        }
        $this->setSequenceValue(self::PGSQL_COUNTRY);
        echo "Import countries end\n";

        echo "Import areas start\n";
        // Create insert areas query
        $insertAreasQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_GEO_AREA, $areas);
        // Try to insert countries
        if (!$insertAreasQuery->execute()) {
            $this->rollback($insertAreasQuery, "Can no insert the areas");
        }
        $this->setSequenceValue(self::PGSQL_GEO_AREA);
        echo "Import areas end\n";

        echo "Import regions start\n";
        // Create insert regions query
        $insertRegionsQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_REGION, $regions);

        // Try to insert regions
        if (!$insertRegionsQuery->execute()) {
            $this->rollback($insertRegionsQuery, "Can no insert the regions");
        }
        $this->setSequenceValue(self::PGSQL_REGION);
        echo "Import regions end\n";

        echo "Import cities start\n";
        // Create cities regions query
        $insertCitiesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_CITY, $cities);
        // Try to insert cities
        if (!$insertCitiesQuery->execute()) {
            $this->rollback($insertCitiesQuery, "Can no insert the cities");
        }
        $this->setSequenceValue(self::PGSQL_CITY);
        echo "Import cities end\n";

        echo "Import addresses start\n";
        // Create cities addresses query
        $insertAddressesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_ADDRESS, $addresses);
        // Try to insert addresses
        if (!$insertAddressesQuery->execute()) {
            $this->rollback($insertAddressesQuery, "Can no insert the addresses");
        }
        $this->setSequenceValue(self::PGSQL_ADDRESS);
        echo "Import addresses end\n";


        $this->commit('Transfer Geo data end');
    }

    /**
     * Action yii import/company
     */
    public function actionCompany()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select companies query
        $companiesQuery = (new Query($mySql))
            ->select([
                'id',
                'geo_area_id' => '(1)',
                'name',
                'description',
                'date_create' => "ADDTIME(date_add, '00:00:00')",
                'legal_name' => 'company_name',
                'requisites' => 'legal_detail',
                'edrpou' => 'SUBSTRING(legal_detail, 0, 14)',
                'type' => "CASE WHEN `entity` = 1 THEN 'organizer' ELSE 'ticket_agency' END",
                'status' => "('active')",
                'sc_id' => '(11)',
            ])
            ->from(self::MYSQL_COMPANY)
            ->where(['IS NOT', 'name', NULL]);
        // Get companies list
        if (empty($companies = $companiesQuery->all())) {
            $this->rollback($companiesQuery, "Countries not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer companies data started');

        // Create insert companies query
        $insertCountriesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_COMPANY, $companies);
        // Try to insert companies
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the companies");
        }

        // Set "system_client" type to kasa.in.ua
        if (!$pgSql->createCommand()->update(self::PGSQL_COMPANY, ['type' => 'system_client', 'sc_id' => null], ['id' => 11])->execute()) {
            $this->rollback($insertCountriesQuery, "Can no update kasa.in.ua");
        }

        // Set set sequence value - max ID from table "company"
        $this->setSequenceValue(self::PGSQL_COMPANY);

        $this->commit('Transfer companies data is done');
    }

    /**
     * Action yii import/user
     */
    public function actionUser()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        $systemCompanyID = $this->getSystemCompanyID();
        $userRoleQuery = (new Query($mySql))
            ->select('user_role.role_id')
            ->from(['user_role' => self::MYSQL_USER_ROLE])
            ->where('user_role.user_id = user.id')
            ->limit(1);

        // Create select users query
        $usersQuery = (new Query($mySql))
            ->select([
                'user.id',
                'company_id' => $userRoleQuery,
                'user.email',
                'password_hash' => 'user.password',
                'user.salt',
                'access_token' => "('access_token')",
                'date_register' => "ADDTIME(user.reg_date, '00:00:00')",
                'first_name' => 'user.name',
                'last_name' => 'user.surname',
                'phone' => 'SUBSTRING(user.phone, 0, 24)',
                'info' => 'user.username',
                'role' => "('cashier')",
                'status' => "CASE WHEN `user`.`status` = 1 THEN ('active') ELSE ('frozen') END",
                'refresh_token' => "('refresh_token')",
                'date_token_expired' => "('01.01.2018 7:10:15')",
                'is_company_owner' => "('f')",
                'print_blank_type' => "('a4')",
            ])
            ->from(['user' => self::MYSQL_USER])
            ->where(['user.type' => 0]);

        if (!empty($this->usersIDs)) {
            $usersQuery->andWhere(['id' => $this->usersIDs]);
        }
        // Get users list
        if (empty($users = $usersQuery->all())) {
            $this->rollback($usersQuery, "Users not found");
        }

        // Check users companies IDs
        foreach ($users as $index => $user) {
            if (empty($user['company_id'])) {
                $users[$index]['company_id'] = $user['company_id'] = $systemCompanyID;
            }
            if ($user['company_id'] != $systemCompanyID) {
                $users[$index]['company_id'] = $this->checkCompanyID($user['company_id']);
            }

            if (empty($user['email'])) {
                $users[$index]['email'] = $this->getRandomEmail($user['id']);
            }

            $users[$index]['old_id'] = $user['id'];
        }

        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer users data started');

        // Write user positions data from sql file
        //$this->writeDataFromFile('positions_data.sql');

        // Set set sequence value - max ID from table "user_position"
        //$this->setSequenceValue(self::PGSQL_USER_POSITION);

        // Create insert users query
        $insertUsersQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_USER, $users);
        // Try to insert users
        if (!$insertUsersQuery->execute()) {
            $this->rollback($insertUsersQuery, "Can no insert the users");
        }

        // Set set sequence value - max ID from table "user"
        $this->setSequenceValue(self::PGSQL_USER);

        // Write users data from sql file
        $this->writeDataFromFile('users_data.sql');

        $this->commit('Transfer users data is done');
    }

    /**
     * Action yii import/passwords
     */
    public function actionPasswords()
    {
        // Get PgSQL Db connection
        $pgSql = Yii::$app->pgSql;


        /** Get data */

        // Create users query
        $usersQuery = (new Query($pgSql))
            ->select([
                'id',
                'password' => 'tmp_password',
                'email',
                'salt',
            ])
            ->from(self::PGSQL_USER)
            ->where(['IS NOT', 'tmp_password', null]);

        // Get users list
        if (empty($users = $usersQuery->all())) {
            $this->rollback($usersQuery, "Users not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Generating users passwords hash started');
        $count = 0;

        // Generate and write users new passwords hashes
        foreach ($users as $user) {
            $updateParams = [
                'password_hash' => $this->generatePasswordHash($user['password'], $user['email'], $user['salt']),
            ];
            $result = $pgSql->createCommand()->update(self::PGSQL_USER, $updateParams, ['id' => $user['id']])->execute();
            if (!$result) {
                $this->rollback(new Query($pgSql), 'Can not write password for user ' . $user['id']);
            } else {
                $count++;
            }
        }

        $this->commit("Generating users passwords hash is done. Updated {$count} records");
    }

    /**
     * Action yii import/client
     */
    public function actionClient()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select clients query
        $clientsQuery = (new Query($mySql))
            ->select([
                'id',
                'sc_id' => "(11)",
                'email',
                'phone' => 'SUBSTRING(phone, 0, 24)',
                'status' => "('unregistered')",
                'date_create' => "ADDTIME(reg_date, '00:00:00')",
                'first_name' => 'name',
                'last_name' => 'surname',
            ])
            ->from(self::MYSQL_USER)
            ->where(['type' => 1]);
        // Get clients list
        if (empty($clients = $clientsQuery->all())) {
            $this->rollback($clientsQuery, "Clients not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer clients data started');

        // Create insert users query
        $insertCountriesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_CLIENT, $clients);
        // Try to insert users
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the clients");
        }

        // Set set sequence value - max ID from table "client"
        $this->setSequenceValue(self::PGSQL_CLIENT);

        $this->commit('Transfer clients data is done');
    }

    /**
     * Action yii import/cash
     */
    public function actionCash()
    {
        // Get DB connections instances
        $dataDb = Yii::$app->dataSql;
        $pgDb = Yii::$app->pgSql;

        /** Get data */
        // Create select cashes query
        $cashesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_CASH);

        // Get cashes list
        if (empty($cashes = $cashesQuery->all())) {
            $this->rollback($cashesQuery, "Cashes not found");
        }

        /** Write data */
        // Begin transaction
        $this->start($pgDb, 'Import cashes data start');

        // Create insert cashes query
        $insertCashesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_CASH, $cashes);
        // Try to insert countries
        if (!$insertCashesQuery->execute()) {
            $this->rollback($insertCashesQuery, "Can no insert the cashes");
        }
        $this->setSequenceValue(self::PGSQL_CASH);


        $this->commit('Import cashes data end');
    }

    /**
     * Action yii import/area
     */
    public function actionArea()
    {
        // Get DB connections instances
        $dataDb = Yii::$app->dataSql;
        $pgDb = Yii::$app->pgSql;

        echo "Import Areas data start\n";

        /** Get data */

        // Create select areas query
        $areasQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_AREA);

        // Get areas list
        if (empty($areas = $areasQuery->all())) {
            $this->rollback($areasQuery, "Areas not found");
        }

        // Create select area files query
        $areaFilesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_HALL);

        // Get areas list
        if (empty($areaFiles = $areaFilesQuery->all())) {
            $this->rollback($areaFilesQuery, "Area files not found");
        }

        // Create select zones query
        $zonesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_ZONE);

        // Get zones list
        if (empty($zones = $zonesQuery->all())) {
            $this->rollback($zonesQuery, "Zones not found");
        }


        // Create select places query
        $placesQuery = (new Query($dataDb))
            ->select('*')
            ->from(self::PGSQL_PLACE);

        // Get places list
        if (empty($places = $placesQuery->all())) {
            $this->rollback($placesQuery, "Places not found");
        }

        /** Write data */
        // Begin transaction
        $this->start($pgDb, 'Import areas start');

        // Create insert areas query
        $insertAreasQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_AREA, $areas);
        // Try to insert countries
        if (!$insertAreasQuery->execute()) {
            $this->rollback($insertAreasQuery, "Can no insert the areas");
        }
        $this->setSequenceValue(self::PGSQL_AREA);
        echo "Import areas end\n";

        echo "Import area files start\n";
        // Create insert area files query
        $insertAreaFilesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_HALL, $areaFiles);
        // Try to insert countries
        if (!$insertAreaFilesQuery->execute()) {
            $this->rollback($insertAreaFilesQuery, "Can no insert the area files");
        }
        $this->setSequenceValue(self::PGSQL_HALL);
        echo "Import area files end\n";

        echo "Import zones start\n";
        // Create insert zones query
        $insertZonesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_ZONE, $zones);
        // Try to insert zones
        if (!$insertZonesQuery->execute()) {
            $this->rollback($insertZonesQuery, "Can no insert the zones");
        }
        $this->setSequenceValue(self::PGSQL_ZONE);
        echo "Import zones end\n";

        echo "Import places start\n";
        // Create insert places query
        $insertPlacesQuery = (new Query($pgDb))->createInsertQuery(self::PGSQL_PLACE, $places);
        // Try to insert places
        if (!$insertPlacesQuery->execute()) {
            $this->rollback($insertPlacesQuery, "Can no insert the places");
        }
        $this->setSequenceValue(self::PGSQL_PLACE);
        echo "Import places end\n";

        $this->commit('Import Areas data end');
    }

    /**
     * Action yii import/event
     */
    public function actionEvent()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select events query
        $eventsQuery = (new Query($mySql))
            ->select([
                'event.id',
                'city_id' => 'area.city_id',
                'area_file_id' => 'event.scheme_id',
                'organizer_id' => 'event.role_id',
                'creator_id' => 'event.user_id',
                'event.name',
                'type' => "('concert')",
                'status' => "(CASE WHEN event.status = 1 THEN ('in_sales') ELSE ('new') END)",
                'barcode_type' => "(CASE WHEN `event`.`barcode_type` = 0 THEN ('code128') ELSE ('ean13') END)",
                'date_create' => '(CASE WHEN event.date_add = \'0000-00-00 00:00:00\' THEN \'2016-10-01 00:00:00\' ELSE ADDTIME(event.date_add, \'00:00:00\') END)',
                'date_start' => '(CASE WHEN timing.start_sale = \'0000-00-00 00:00:00\' THEN \'2016-10-01 00:00:00\' ELSE ADDTIME(timing.start_sale, \'00:00:00\') END)',
                'date_end' => '(CASE WHEN timing.stop_sale = \'0000-00-00 00:00:00\' THEN \'2016-10-01 00:00:00\' ELSE ADDTIME(timing.stop_sale, \'00:00:00\') END)',
                'date_entry' => '(CASE WHEN timing.entrance = \'0000-00-00 00:00:00\' THEN \'2016-10-01 00:00:00\' ELSE ADDTIME(timing.entrance, \'00:00:00\') END)',
                'date_sales_start' => '(CASE WHEN event.start_sale = \'0000-00-00 00:00:00\' THEN \'2016-10-01 00:00:00\' ELSE ADDTIME(event.start_sale, \'00:00:00\') END)',
                'date_sales_end' => '(CASE WHEN event.end_sale = \'0000-00-00 00:00:00\' THEN \'2016-10-01 00:00:00\' ELSE ADDTIME(event.end_sale, \'00:00:00\') END)',
                'description' => 'event.description_id',
                'url_alias' => 'event.url',
                'html_caption' => 'event.html_header',
                'event.keywords',
                'event.meta_description',
                'system_name' => 'event.sys_name',
                'in_main_slider' => "(CASE WHEN `event`.`slider_main` = 0 THEN ('f') ELSE ('t') END)",
                'in_city_slider' => "(CASE WHEN `event`.`slider_city` = 0 THEN ('f') ELSE ('t') END)",
                'in_main_page' => "('f')",
            ])
            ->from(['event' => self::MYSQL_EVENT])
            ->leftJoin(['timing' => self::MYSQL_EVENT_TIMING], 'timing.event_id = event.id AND timing.status = 1')
            ->leftJoin(['scheme' => self::MYSQL_SCHEME], 'scheme.id = event.scheme_id')
            ->leftJoin(['area' => self::MYSQL_AREA], 'area.id = scheme.location_id');
        if (($eventIDs = $this->getEventsIds()) !== null) {
            $eventsQuery->where(['event.id' => $eventIDs]);
        }

        // Get clients list
        if (empty($events = $eventsQuery->all())) {
            $this->rollback($eventsQuery, "Events not found");
        }

        $systemCompanyID = $this->getSystemCompanyID();
        foreach ($events as $id => $event) {
            $events[$id]['creator_id'] = $this->checkUserID($event['creator_id']);
            if (empty($event['organizer_id'])) {
                $events[$id]['organizer_id'] = $event['organizer_id'] = $systemCompanyID;
            }
            if ($event['organizer_id'] != $systemCompanyID) {
                $events[$id]['organizer_id'] = $this->checkCompanyID($event['organizer_id']);
            }
        }

        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer events data started');

        // Create insert users query
        $insertEventsQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_EVENT, $events);
        // Try to insert users
        if (!$insertEventsQuery->execute()) {
            $this->rollback($insertEventsQuery, "Can no insert the events");
        }

        // Set set sequence value - max ID from table "event"
        $this->setSequenceValue(self::PGSQL_EVENT);

        $this->commit('Transfer events data is done', true);
    }

    /**
     * Action yii import/media
     */
    public function actionMedia()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select media query
        $mediaQuery = (new Query($mySql))
            ->select([
                'id',
                'entity_id' => 'event_id'  ,
                'entity_class' => "('models\\\\Event')",
                'path' => "CONCAT(('poster/'), `event_id`, ('/'), `file`)",
                'type' => "('image')",
                'status' => "CASE WHEN `status` = 0 THEN 'main' ELSE 'media' END",
            ])
            ->where(['IS NOT', 'event_id', null])
            ->from(self::MYSQL_MEDIA);
        if (($eventIDs = $this->getEventsIds()) !== null) {
            $mediaQuery->andWhere(['event_id' => $eventIDs]);
        }
        // Get media list
        if (empty($medias = $mediaQuery->all())) {
            $this->rollback($mediaQuery, "Media not found");
        }

        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer media data started');

        // Create insert media query
        $insertMediaQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_MEDIA, $medias);
        // Try to insert media
        if (!$insertMediaQuery->execute()) {
            $this->rollback($insertMediaQuery, "Can no insert the media");
        }

        // Set set sequence value - max ID from table "media"
        $this->setSequenceValue(self::PGSQL_MEDIA);

        $this->commit('Transfer media data is done', true);
    }

    /**
     * Action yii import/order
     */
    public function actionOrder()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        // Try to get orders from cache
        if (($orders = $this->getCache('orders')) === null) {
            $ticketsSql = (new Query($mySql))
                ->from(['ticket' => self::MYSQL_TICKET])
                ->where('ticket.order_id = `order`.`id`');

            // Create ticket query
            $ticketStatusQuery = clone $ticketsSql;
            $ticketStatusSql = $ticketStatusQuery->select('ticket.pay_status')->limit(1)->asString();

            // Get tickets orders IDs query
            $ordersIDsQuery = clone $ticketsSql;
            $ordersIDsQuery->where = null;
            $ordersIDsQuery
                ->select(['order_id' => 'DISTINCT(ticket.order_id)'])
                ->indexBy('order_id');

            if (($eventIDs = $this->getEventsIds()) !== null) {
                $ordersIDsQuery->andWhere(['ticket.event_id' => $eventIDs]);
            }

            // Get orders ids
            if (!empty($ordersIDs = array_keys($ordersIDsQuery->all()))) {
                $ordersIDsString = implode(',', $ordersIDs);
            }

            // Ticket pay status
            $statusNotPayed = self::PAYMENT_STATUS_NOT_PAYED;

            $systemCompanyID = $this->getSystemCompanyID();
            $adminID = $this->getAdminID();

            // Create select orders query
            $ordersQuery = (new Query($mySql))
                ->select([
                    'order.id',
                    'creator_id' => "CASE WHEN `user`.`type` = 0 THEN `order`.`user_id` ELSE ({$adminID}) END",
                    'seller_id' => "CASE WHEN `user`.`type` = 0 THEN `order`.`user_id` ELSE ({$adminID}) END",
                    'partner_id' => "CASE WHEN `user`.`type` = 0 THEN `order`.`role_id` ELSE ({$systemCompanyID}) END",
                    'status' => "CASE WHEN ({$ticketStatusSql})={$statusNotPayed} THEN ('new') ELSE ('past') END",
                    'date_create' => "(CASE WHEN `order`.`date_add` = '0000-00-00 00:00:00' THEN '2016-10-01 00:00:00' ELSE ADDTIME(`order`.`date_add`, '00:00:00') END)",
                    'date_close' => "(CASE WHEN `order`.`date_update` = '0000-00-00 00:00:00' THEN '2016-10-01 00:00:00' ELSE ADDTIME(`order`.`date_update`, '00:00:00') END)",
                    'description' => 'order.comment',
                ])
                ->from(['order' => self::MYSQL_ORDER])
                ->leftJoin(['user' => self::MYSQL_USER], '`user`.`id` = `order`.`user_id`');
            if (isset($ordersIDsString)) {
                $ordersQuery->andWhere("`order`.`id` IN({$ordersIDsString})");
            }

            // Get orders
            if (empty($orders = $ordersQuery->all())) {
                $this->rollback($ordersQuery, "Orders not found");
            }

            // Execute orders data (check constraint keys)
            foreach ($orders as $index => $order) {
                // Check order creator
                $orders[$index]['creator_id'] = $orders[$index]['seller_id'] = $this->checkUserID($order['creator_id']);

                // Try to find order partner company
                if ($order['partner_id'] != $systemCompanyID) {
                    $orders[$index]['partner_id'] = $order['partner_id'] = $this->checkCompanyID($order['partner_id']);
                }
            }

            $this->setCache('orders', $orders);
        }

        // Begin transaction
        $this->start($pgSql, 'Transfer orders data start');

        // Write orders data
        $this->writeBigData(self::PGSQL_ORDER, $orders);

        // Set order table ID sequence value
        $this->setSequenceValue(self::PGSQL_ORDER);

        // Commit the transaction
        $this->commit('Transfer orders data end', true);
    }

    /**
     * Action yii import/quota
     */
    public function actionQuota()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        // Try to get quotas form cache
        if (($quotas = $this->getCache('quotas')) === null) {
            /** Get data */
            // Create quota creator ID query
            $creatorIDQuery = (new Query($mySql))
                ->select('creator_company.user_id')
                ->from(['creator_company' => self::MYSQL_USER_COMPANY])
                ->where('creator_company.role_id = quota.role_from_id')
                ->limit(1);

            // Quota statuses and types variables
            $typeEQuota = self::QUOTA_TYPE_E_QUOTA;

            // Create quota ticket query
            $quotaTicketSql = (new Query($mySql))
                ->select('ticket.id')
                ->from(['ticket' => self::MYSQL_TICKET])
                ->where('ticket.order_id = quota.order_id')
                ->limit(1)
                ->asString();

            // Create select quotas query
            $quotasQuery = (new Query($mySql))
                ->select([
                    'quota.id',
                    'quota.event_id',
                    'partner_id' => 'quota.role_to_id',
                    'creator_id' => $creatorIDQuery,
                    'quota.name',
                    'type' => "(CASE WHEN quota.type = {$typeEQuota} THEN ('e_quota') ELSE ('quota') END)",
                    'status' => "(CASE WHEN ($quotaTicketSql) IS NULL THEN ('new') ELSE ('active') END)",
                    'date_create' => "(CASE WHEN `order`.`date_add` = '0000-00-00 00:00:00' THEN '2016-10-01 00:00:00' ELSE ADDTIME(`order`.`date_add`, '00:00:00') END)",
                    'date_close' => "(CASE WHEN `order`.`date_update` = '0000-00-00 00:00:00' THEN '2016-10-01 00:00:00' ELSE ADDTIME(`order`.`date_update`, '00:00:00') END)",
                    'partner_fee_type' => "('percent')",
                    'partner_fee' => 'quota.percent',
                    'description' => 'quota.comment',
                    'provider_id' => 'quota.role_from_id',
                ])
                ->from(['quota' => self::MYSQL_QUOTA])
                ->leftJoin(['order' => self::MYSQL_ORDER], '`order`.`id` = quota.order_id');
            if (($eventIDs = $this->getEventsIds()) !== null) {
                $quotasQuery->andWhere(['quota.event_id' => $eventIDs]);
            }
            // Try to get quotas
            if (empty($quotas = $quotasQuery->all())) {
                $this->rollback($quotasQuery, 'Quotas not found');
            }

            $systemCompanyID = $this->getSystemCompanyID();

            // Execute quotas data (check constraint keys)
            foreach ($quotas as $index => $quota) {
                // Check quota user
                $quotas[$index]['creator_id'] = $this->checkUserID($quota['creator_id']);

                // Check quota partner
                if (empty($quota['partner_id'])) {
                    $quotas[$index]['partner_id'] = $quota['partner_id'] = $systemCompanyID;
                }
                if ($quota['partner_id'] != $systemCompanyID) {
                    $quotas[$index]['partner_id'] = $this->checkCompanyID($quota['partner_id']);
                }

                // Check quota provider
                if (empty($quota['provider_id'])) {
                    $quotas[$index]['provider_id'] = $quota['provider_id'] = $systemCompanyID;
                }
                if ($quota['provider_id'] != $systemCompanyID) {
                    $quotas[$index]['provider_id'] = $this->checkCompanyID($quota['provider_id']);
                }
            }

            $this->setCache('quotas', $quotas);
        }


        /** Write data */
        // Begin transaction
        $this->start($pgSql, 'Import quotas data start');

        // Create quotas insert query
        $quotasInsertQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_QUOTA, $quotas);
        // Try to write quotas
        if (!$quotasInsertQuery->execute()) {
            $this->rollback($quotasInsertQuery, 'Can not save the quotas');
        }

        $this->setSequenceValue(self::PGSQL_QUOTA);

        // Commit the transaction
        $this->commit('Import quotas data end', true);
    }

    /**
     * Action yii import/read-zones-info
     */
    public function actionReadZonesInfo()
    {
        echo 'Reading zones info is start';

        $zonesQuery = (new Query(Yii::$app->mySql))
            ->select([
                'place.event_id',
                'zone_id' => 'place.sector_id',
            ])
            ->from(['place' => self::MYSQL_PLACE])
            ->groupBy(['place.event_id', 'place.sector_id']);
        if (($eventIDs = $this->getEventsIds()) !== null) {
            $zonesQuery->andWhere(['place.event_id' => $eventIDs]);
        }

        foreach ($zonesQuery->each() as $zone) {
            $this->getZoneInfo($zone['event_id'], $zone['zone_id']);
        }

        $this->commit('Reading zones info is done', true);
    }

    /**
     * Action yii import/ticket
     */
    public function actionTicket()
    {
        $pgSql = Yii::$app->pgSql;

        $eventIDs = $this->getEventsIds();
        if (count($eventIDs) <= 25) {
            $this->writeTickets();
            $this->commit(null, true);
            return;
        }

        $limit = 25;
        $iterationsCount = round(count($eventIDs) / $limit) + 1;
        for ($i = 0; $i < $iterationsCount; $i++) {
            $subData = array_slice($eventIDs, $i*$limit, $limit);
            if (empty($subData)) {
                break;
            }
            $this->eventsIDs = $subData;
            $this->writeTickets($i);
        }

        $this->eventsIDs = $eventIDs;

        // Remove orders who has no related tickets
        $orderIDQuery = (new Query($pgSql))
            ->select('DISTINCT(order_id)')
            ->from(self::PGSQL_TICKET);
        $pgSql->createCommand()->delete(self::PGSQL_ORDER, ['NOT IN', 'id', $orderIDQuery])->execute();

        $this->commit('All tickets are imported', true);
    }
    private function writeTickets($iterationNumber = 0)
    {
        $adminID = $this->getAdminID();
        $pgSql = Yii::$app->pgSql;

        // Try to get tickets from cache
        if (($tickets = $this->getCache('tickets_' . $iterationNumber)) === null) {
            // Create tickets query
            $ticketsQuery = $this->createSelectMysqlTicketsQuery(false);

            // Try to find tickets
            if (empty($tickets = $ticketsQuery->all())) {
                $this->rollback($ticketsQuery, 'Tickets not found');
            }

            // Execute tickets (delete copies)
            $ticketsIDs = [];
            $placesCopies = [];
            foreach ($tickets as $index => $ticket) {
                if (in_array($ticket['id'], $ticketsIDs) || $ticket['hall_id'] != $ticket['place_hall_id']) {
                    unset($tickets[$index]);
                    continue;
                }
                $ticketsIDs[] = $ticket['id'];

                if (empty($ticket['number'])) {
                    continue;
                }
                // Create index for search copies
                $copyIndex = $ticket['event_id'] . ':' . $ticket['zone_id'] . ':' . $ticket['row_number'] . ':' . $ticket['number'];

                if (!isset($placesCopies[$copyIndex])) {
                    $placesCopies[$copyIndex] = [
                        'id' => $ticket['id'],
                        'ticket_id' => $ticket['ticket_id'],
                        'index' => $index,
                    ];
                    continue;
                }

                if ($ticket['ticket_id'] == null && $placesCopies[$copyIndex]['ticket_id'] == null) {
                    if ($placesCopies[$copyIndex]['id'] >= $ticket['id']) {
                        unset($tickets[$index]);
                    } else {
                        unset($tickets[$placesCopies[$copyIndex]['index']]);
                        $placesCopies[$copyIndex] = [
                            'id' => $ticket['id'],
                            'index' => $index,
                        ];
                    }
                } elseif ($ticket['ticket_id'] == null) {
                    unset($tickets[$index]);
                } elseif ($placesCopies[$copyIndex]['ticket_id'] == null) {
                    unset($tickets[$placesCopies[$copyIndex]['index']]);
                    $placesCopies[$copyIndex] = [
                        'id' => $ticket['id'],
                        'index' => $index,
                    ];
                }
            }
            unset($ticketsIDs, $placesCopies);

            /** Write data */
            // Begin transaction
            $this->start($pgSql, 'Import tickets data start');

            // Execute tickets (check foreign keys and find real zones and places IDs)
            foreach ($tickets as $index => $ticket) {
                $zoneID = null;

                if ($ticket['event_id'] == 1728 && $ticket['hall_id'] == 328 && $ticket['zone_id'] == 2350) {
                    $zoneID = 591;
                }

                if (!isset($zoneID)) {
                    // Create zone ID query
                    $zoneIDQuery = (new Query($pgSql))
                        ->select('id')
                        ->from(self::PGSQL_ZONE)
                        ->where(['area_file_id' => $ticket['hall_id']]);

                    // Try to find zone by front_id
                    $zoneID = $zoneIDQuery->andWhere(['front_id' => $ticket['zone_id']])->scalar();

                    if (empty($zoneID)) {
                        $zoneID = $zoneIDQuery->where([
                            'area_file_id' => $ticket['hall_id'],
                            'name' => $ticket['zone_name'],
                        ])->scalar();
                    }
                    if (empty($zoneID)) {
                        // Zone not found - write error log
                        $this->writeImportErrorLog($ticket, 'zone');
                        unset($tickets[$index]);
                        continue;
                    }
                }

                unset($tickets[$index]['zone_name']);

                // Set real zone ID
                $tickets[$index]['zone_id'] = $zoneID;

                // Set correct ticket status
                $tickets[$index]['status'] = $ticket['status'] = $this->getTicketStatus($ticket);

                // Check ticket order ID
                if ($ticket['order_id'] !== null) {
                    // Check order ID
                    $orderID = $this->checkOrderID($ticket['order_id']);
                    if ($orderID === null || in_array($ticket['status'], ['new', 'in_quota', 'in_partner_sales'])) {
                        $tickets[$index]['order_id'] = null;
                    }
                }

                // Try to find real place ID
                if ($ticket['number'] > 0) {
                    $placeID = (new Query($pgSql))
                        ->select('id')
                        ->from(self::PGSQL_PLACE)
                        ->where([
                            'zone_id' => $zoneID,
                            'row_number' => $ticket['row_number'],
                            'number' => $ticket['number'],
                        ])
                        ->scalar();
                } else {
                    $placeID = null;
                }

                // Check place ID
                if (empty($placeID) && $ticket['number'] > 0) {
                    // Place not found - write error log
                    $this->writeImportErrorLog($ticket, 'place');
                    unset($tickets[$index]);
                    continue;
                }

                // Check printer and seller IDs
                $tickets[$index]['printer_id'] = $this->checkUserID($ticket['printer_id']);
                if (!empty($ticket['company_id']) && !empty($ticket['seller_id']) && $ticket['seller_id'] != $adminID) {
                    $tickets[$index]['seller_id'] = $this->checkUser($ticket['seller_id'], $ticket['company_id']);
                } else {
                    $tickets[$index]['seller_id'] = $this->checkUserID($ticket['seller_id']);
                }
                unset($tickets[$index]['company_id']);


                // Set real place ID
                $tickets[$index]['place_id'] = $placeID ?: null;

                // Check visitor info
                if (!empty($ticket['visitor_email']) || !empty($ticket['visitor_phone'])) {
                    // Create visitor query
                    $visitorQuery = (new Query($pgSql))
                        ->select('id')
                        ->from(self::PGSQL_CLIENT)
                        ->limit(1);
                    if (!empty($ticket['visitor_email'])) {
                        $visitorQuery->where(['email' => $ticket['visitor_email']]);
                    }
                    if (!empty($ticket['visitor_phone'])) {
                        $visitorQuery->andWhere(['phone' => $ticket['visitor_phone']]);
                    }

                    // Try to find visitor ID
                    $visitorID = $visitorQuery->scalar();

                    if (empty($visitorID)) {
                        $firstName = $middleName = $lastName = null;
                        if (!empty($ticket['visitor_name'])) {
                            $visitorName = $ticket['visitor_name'];
                            $names = explode(' ', $ticket['visitor_name']);
                            $firstName = array_shift($names);
                            if (count($names) === 1) {
                                $lastName = array_shift($names);
                            } elseif (count($names) === 2) {
                                $middleName = array_shift($names);
                                $lastName = array_shift($names);
                            }
                        }
                        // If visitor is not exist - create them
                        $insertData = [
                            'sc_id' => self::KASA_IN_UA_ID,
                            'email' => !empty($ticket['visitor_email']) ? $ticket['visitor_email'] : null,
                            'phone' => !empty($ticket['visitor_phone']) ? $ticket['visitor_phone'] : null,
                            'first_name' => $firstName,
                            'middle_name' => $middleName,
                            'last_name' => $lastName,
                        ];
                        // Insert visitor data
                        $pgSql->createCommand()->insert(self::PGSQL_CLIENT, $insertData)->execute();
                        // Get visitor ID
                        $visitorID = $pgSql->getLastInsertID($this->sequenceName(self::PGSQL_CLIENT));
                        $this->setSequenceValue(self::PGSQL_CLIENT);
                    }
                }
                $tickets[$index]['visitor_id'] = !empty($visitorID) ? $visitorID : null;
                unset($tickets[$index]['visitor_email'], $tickets[$index]['visitor_phone'], $tickets[$index]['visitor_name']);

                // Check payment info
                if (isset($ticket['payment_status'])) {
                    switch ($ticket['payment_status']) {
                        case self::PAYMENT_STATUS_PAID:
                            $status = 'paid';
                            break;
                        case self::PAYMENT_STATUS_INVITE:
                            $status = 'invite';
                            break;
                        default:
                            $status = 'not_paid';
                            break;
                    }

                    if ($ticket['price'] == 1) {
                        $status = 'invite';
                    }

                    switch ($ticket['payment_type']) {
                        case self::PAYMENT_TYPE_CASH_PICKUP:
                        case self::PAYMENT_TYPE_CASH_PICKUP_2:
                            $paymentType = 'cash';
                            break;
                        case self::PAYMENT_TYPE_CARD_PICKUP:
                        case self::PAYMENT_TYPE_CARD_PICKUP_2:
                            $paymentType = 'credit_card';
                            break;
                        case self::PAYMENT_TYPE_CARD_POST_SERVICE:
                            $paymentType = 'credit_card';
                            break;
                        case self::PAYMENT_TYPE_CASH_POST_SERVICE:
                            $paymentType = 'cash';
                            break;
                        default:
                            $paymentType = 'credit_card';
                            break;
                    }

                    // Create payment insert data
                    $insertData = [
                        'recipient_id' => $this->getMoneyRecipientID($ticket),
                        'sum' => $ticket['price'] * (1 - $ticket['discount'] / 100),
                        'date_sent' => $ticket['date_sold'],
                        'status' => $status,
                        'type' => $paymentType,
                    ];

                    // Insert payment data
                    $pgSql->createCommand()->insert(self::PGSQL_PAYMENT, $insertData)->execute();
                    $this->setSequenceValue(self::PGSQL_PAYMENT);
                    // Get payment ID
                    $paymentID = $pgSql->getLastInsertID($this->sequenceName(self::PGSQL_PAYMENT));
                }
                $tickets[$index]['payment_id'] = !empty($paymentID) ? $paymentID : null;
                unset($tickets[$index]['payment_type'], $tickets[$index]['payment_status']);

                // Check delivery info
                if (isset($ticket['delivery_status'])) {
                    switch ($ticket['delivery_status']) {
                        case self::DELIVERY_STATUS_SENT:
                            $status = 'current';
                            break;
                        case self::DELIVERY_STATUS_RECEIVED:
                        case self::DELIVERY_STATUS_SENT_ON_EMAIL:
                            $status = 'past';
                            break;
                        case self::DELIVERY_STATUS_RETURNED:
                            $status = 'returned';
                            break;
                        default:
                            $status = 'future';
                            break;
                    }

                    switch ($ticket['delivery_type']) {
                        case self::DELIVERY_TYPE_PICKUP:
                        case self::DELIVERY_TYPE_PICKUP_2:
                            $deliveryType = 'pickup';
                            break;
                        case self::DELIVERY_TYPE_POST_SERVICE:
                        case self::DELIVERY_TYPE_POST_SERVICE_2:
                            $deliveryType = 'post_service';
                            break;
                        case self::DELIVERY_TYPE_COURIER:
                        case self::DELIVERY_TYPE_COURIER_2:
                            $deliveryType = 'courier';
                            break;
                        default:
                            $deliveryType = 'internet';
                            break;
                    }

                    // Check delivery address
                    if (!empty($ticket['delivery_address'])) {
                        // Trt to find addressID
                        $addressQuery = (new Query($pgSql))
                            ->select('id')
                            ->from(self::PGSQL_ADDRESS)
                            ->limit(1);
                        if (!empty($ticket['delivery_city_id'])) {
                            $addressQuery->where(['city_id' => $ticket['delivery_city_id']]);
                        }
                        if (!empty($ticket['delivery_address'])) {
                            $addressQuery->andWhere(['address' => $ticket['delivery_address']]);
                        }
                        if (empty($addressID = $addressQuery->scalar()) && !empty($ticket['delivery_city_id'])) {
                            // If address not found - create them
                            $insertData = [
                                'city_id' => $ticket['delivery_city_id'],
                                'address' => $ticket['delivery_address'],
                            ];
                            // Insert payment data
                            $pgSql->createCommand()->insert(self::PGSQL_ADDRESS, $insertData)->execute();
                            $this->setSequenceValue(self::PGSQL_ADDRESS);
                            // Get payment ID
                            $addressID = $pgSql->getLastInsertID($this->sequenceName(self::PGSQL_ADDRESS));
                        }
                    }

                    // Create payment insert data
                    $insertData = [
                        'status' => $status,
                        'type' => $deliveryType,
                        'date_planned' => self::EVENT_START_DATE,
                    ];

                    if (!empty($addressID)) {
                        $insertData['address_id'] = $addressID;
                    }
                    if (!empty($tickets['seller_id'])) {
                        $insertData['sender_id'] = $tickets['seller_id'];
                    }
                    if (!empty($visitorName)) {
                        $insertData['receiver_name'] = $visitorName;
                    }

                    // Insert payment data
                    $pgSql->createCommand()->insert(self::PGSQL_DELIVERY, $insertData)->execute();
                    $this->setSequenceValue(self::PGSQL_DELIVERY);
                    // Get payment ID
                    $deliveryID = $pgSql->getLastInsertID($this->sequenceName(self::PGSQL_DELIVERY));
                }
                $tickets[$index]['delivery_id'] = !empty($deliveryID) ? $deliveryID : null;

                unset($tickets[$index]['delivery_type'], $tickets[$index]['delivery_status'], $tickets[$index]['delivery_city_id'], $tickets[$index]['delivery_address']);
                unset($tickets[$index]['row_number'], $tickets[$index]['number'], $tickets[$index]['hall_id'], $tickets[$index]['place_hall_id']);
                unset($tickets[$index]['pay_status'], $tickets[$index]['quota_id'], $tickets[$index]['ticket_id']);
                unset($deliveryID, $paymentID, $visitorID, $placeID);
            }

            $this->setCache('tickets_' . $iterationNumber, $tickets);
        } else {
            return;
        }

        // Try to write tickets
        // $this->writeBigData(self::PGSQL_TICKET, $tickets);
        $insertQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_TICKET, $tickets);
        if (!$insertQuery->execute()) {
            $this->rollback($insertQuery, 'Can not write data to ticket');
        }

        // Set tickets sequence
        $this->setSequenceValue(self::PGSQL_TICKET);

        // Commit transaction
        $this->commit('Import tickets data end');
    }

    /**
     * Action yii import/quota-ticket
     */
    public function actionQuotaTicket()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        // Select tickets
        $ticketsQuery = (new Query($mySql))
            ->select([
                'quota_id' => 'quota.id',
                'ticket_id' => 'ticket.place_id',
            ])
            ->from(['ticket' => self::MYSQL_TICKET])
            ->leftJoin(['quota' => self::MYSQL_QUOTA], 'quota.order_id = ticket.order_id')
            ->where('quota.id IS NOT NULL AND ticket.status NOT IN(:returned, :returned_from_quota)')
            ->addParams([
                'returned' => self::TICKET_STATUS_CANCELLED,
                'returned_from_quota' => self::TICKET_STATUS_RETURNED_FROM_QUOTA,
            ]);
        if (($eventIDs = $this->getEventsIds()) !== null) {
            $ticketsQuery->andWhere(['ticket.event_id' => $eventIDs]);
        }

        $quotaAndTicketsIDs = [];

        // Create quota tickets data array
        $quotaTicketsData = [];
        foreach ($ticketsQuery->each() as $ticket) {
            // Try to find pgSql ticket ID
            $pgSqlTicketID = (new Query($pgSql))
                ->select('id')
                ->from(self::PGSQL_TICKET)
                ->where(['id' => $ticket['ticket_id']])
                ->scalar();
            if (empty($pgSqlTicketID)) {
                continue;
            }

            // Add quota and ticket IDs to special array (no copies!)
            $IDsIndex = $ticket['quota_id'] . ':' . $ticket['ticket_id'];
            if (in_array($IDsIndex, $quotaAndTicketsIDs)) {
                continue;
            }
            $quotaAndTicketsIDs[] = $IDsIndex;
            // Add quota and tickets IDs to quota_ticket data table
            $quotaTicketsData[] = $ticket;
        }

        if (empty($quotaTicketsData)) {
            return;
        }

        $this->start($pgSql, 'Quota ticket data import start');
        // Create insert in quota ticket query
        $createQuotaTicketQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_QUOTA_TICKET, $quotaTicketsData);
        // Try to save quota tickets data
        if (!$createQuotaTicketQuery->execute()) {
            $this->rollback($createQuotaTicketQuery, 'Can not write quota tickets');
        }

        // Commit the transaction
        $this->commit('Quota ticket data import end', true);
    }

    /**
     * Action yii import/ticket-story
     */
    public function actionTicketStory()
    {
        $eventIDs = $this->getEventsIds();
        if (count($eventIDs) <= 25) {
            $this->writeTicketStory();
            $this->commit(null, true);
            return;
        }

        $limit = 25;
        $iterationsCount = round(count($eventIDs) / $limit) + 1;

        for ($i = 0; $i < $iterationsCount; $i++) {
            $subData = array_slice($eventIDs, $i*$limit, $limit);
            if (empty($subData)) {
                break;
            }
            $this->eventsIDs = $subData;
            $this->writeTicketStory($i);
        }

        $this->eventsIDs = $eventIDs;
        $this->commit('All tickets stories are imported', true);
    }
    private function writeTicketStory($iterationNumber = 0)
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** GET data */

        if (($ticketsStoryData = $this->getCache('story_tickets_' . $iterationNumber)) === null) {
            // Create tickets query
            $ticketsQuery = $this->createSelectMysqlTicketsQuery();

            $ticketsStoryData = [];
            foreach ($ticketsQuery->each() as $ticket) {
                // Get ticket story status
                $status = $this->getTicketStatus($ticket);

                // Set correct story action and ticket status (by imported ticket status)
                switch ($status) {
                    case 'new':
                    case 'returned':
                    case 'canceled':
                        if (in_array($ticket['status'], [self::TICKET_STATUS_CANCELLED, self::TICKET_STATUS_RETURNED_FROM_QUOTA])) {
                            $action = 'returned';
                            $status = 'returned';
                        } else {
                            $action =  'created';
                        }
                        break;
                    case 'in_quota':
                        $action = 'add_to_quota';
                        break;
                    case 'in_sales':
                        $action = 'add_to_cart';
                        break;
                    case 'reserved':
                        $action = 'reserved';
                        break;
                    case 'sold':
                    case 'in_partner_sales':
                        $action = 'sold';
                        break;
                    default:
                        $action = 'updated';
                        break;
                }

                // Try to find pgSql ticket
                $pgSqlTicket = (new Query($pgSql))
                    ->select([
                        'zone_id',
                        'place_id',
                        'visitor_id',
                        'delivery_id',
                        'payment_id',
                    ])
                    ->from(self::PGSQL_TICKET)
                    ->where(['id' => $ticket['id']])
                    ->one();
                if (empty($pgSqlTicket)) {
                    continue;
                }

                $ticketsStoryData[] = [
                    'ticket_id' => $ticket['id'],
                    'actor_id' => $this->checkUserID($ticket['seller_id']),
                    'action' => $action,
                    'date' => empty($ticket['date_sold']) || $ticket['date_sold'] == '0000-00-00 00:00:00' ? '2017-01-01 00:00:00' : $ticket['date_sold'],
                    'event_id' => $ticket['event_id'],
                    'zone_id' => $pgSqlTicket['zone_id'],
                    'place_id' => $pgSqlTicket['place_id'],
                    'order_id' => $ticket['order_id'] !== null ? $this->checkOrderID($ticket['order_id']) : null,
                    'printer_id' => $this->checkUserID($ticket['printer_id']),
                    'seller_id' => $ticket['seller_id'],
                    'price' => $ticket['price'],
                    'type' => 'ticket',
                    'status' => $status,
                    'blank_type' => $ticket['blank_type'],
                    'print_status' => empty($ticket['date_print']) || $ticket['date_print'] == '0000-00-00 00:00:00' ? 'not_printed' : 'printed',
                    'date_print' => $ticket['date_print'],
                    'date_sold' => $ticket['date_sold'],
                    'barcode' => $ticket['barcode'],
                    'visitor_id' => $pgSqlTicket['visitor_id'],
                    'discount' => $ticket['discount'],
                    'payment_id' => empty($ticket['date_sold']) || $ticket['date_sold'] == '0000-00-00 00:00:00' ? null : $pgSqlTicket['payment_id'],
                    'delivery_id' => $pgSqlTicket['delivery_id'],
                    'quota_id' => $ticket['status'] == self::TICKET_STATUS_RETURNED_FROM_QUOTA ? $this->checkQuotaID($ticket['quota_id']) : null,
                ];
            }

            $this->setCache('story_tickets_' . $iterationNumber, $ticketsStoryData);
        } else {
            return;
        }
        if (empty($ticketsStoryData)) {
            return;
        }


        /** Write data */
        // Begin the transaction
        $this->start($pgSql, 'Import tickets story data start');

        // Try to write tickets story
        //$this->writeBigData(self::PGSQL_TICKET_STORY, $ticketsStoryData);
        $insertQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_TICKET_STORY, $ticketsStoryData);
        if (!$insertQuery->execute()) {
            $this->rollback($insertQuery, 'Can not write data to story_ticket');
        }

        // Set tickets story current id sequence value
        $this->setSequenceValue(self::PGSQL_TICKET_STORY);

        // commit the transaction
        $this->commit('Import tickets story data end');
    }

    /**
     * Action yii import/recipient-id
     */
    public function actionRecipientId()
    {
        $pgSql = Yii::$app->pgSql;
        $mySql = Yii::$app->mySql;

        $this->start($pgSql, 'Write payment recipients IDs started');

        $paymentsQuery = (new Query($pgSql))
            ->select([
                'payment.id',
                'ticket_id' => 'ticket.id',
            ])
            ->from(['payment' => self::PGSQL_PAYMENT])
            ->leftJoin(['ticket' => self::PGSQL_TICKET], 'ticket.payment_id = payment.id');

        foreach ($paymentsQuery->each() as $payment) {
            $tickets = (new Query($mySql))
                ->select([
                    'id',
                    'payment_type' => 'pay_type',
                    'printer_id' => 'author_print_id',
                    'payment_status' => 'pay_status',
                ])
                ->from(self::MYSQL_TICKET)
                ->where(['place_id' => $payment['ticket_id']])
                ->all();
            if (empty($tickets)) {
                continue;
            }

            $realTicket = null;
            foreach ($tickets as $ticket) {
                if ($realTicket === null || $ticket['id'] > $realTicket['id']) {
                    $realTicket = $ticket;
                }
            }

            if (($recipientID = $this->getMoneyRecipientID($realTicket)) === null) {
                continue;
            }

            // Check the user
            $recipientID = $this->checkUserID($recipientID);

            $pgSql->createCommand()->update(self::PGSQL_PAYMENT, ['recipient_id' => $recipientID], ['id' => $payment['id']])->execute();
        }

        $this->commit('Write payment recipients IDs done');
    }

    /**
     * Action yii import/fix-tickets
     */
    public function actionFixTickets()
    {
        $pgSql = Yii::$app->pgSql;
        $mySql = Yii::$app->mySql;

        $adminID = $this->getAdminID();

        $ticketsQuery = (new Query($pgSql))
            ->select([
                'id',
                'printer_id',
                'seller_id'
            ])
            ->from(self::PGSQL_TICKET)
            ->where('printer_id = :admin_id OR seller_id = :admin_id')
            ->addParams(['admin_id' => $adminID]);

        $this->start($pgSql, 'Start fixing tickets');

        foreach ($ticketsQuery->each() as $ticket) {
            $oldTickets = (new Query($mySql))
                ->select([
                    'ticket.id',
                    'printer_id' => 'ticket.author_print_id',
                    'seller_id' => "CASE WHEN user.type = 0 THEN ticket.user_id ELSE ({$adminID}) END",
                ])
                ->from(['ticket' => self::MYSQL_TICKET])
                ->leftJoin(['user' => self::MYSQL_USER], 'user.id = ticket.user_id')
                ->where(['place_id' => $ticket['id']])
                ->all();
            if (empty($oldTickets)) {
                continue;
            }

            $realTicket = null;
            foreach ($oldTickets as $oldTicket) {
                if ($realTicket === null || $oldTicket['id'] > $realTicket['id']) {
                    $realTicket = $oldTicket;
                }
            }

            $updateParams = [];
            if ($ticket['printer_id'] != $realTicket['printer_id']) {
                $updateParams['printer_id'] = $this->checkUserID($realTicket['printer_id']);
            }
            if ($ticket['seller_id'] != $realTicket['seller_id']) {
                $updateParams['seller_id'] = $this->checkUserID($realTicket['seller_id']);
            }

            if (!empty($updateParams)) {
                $pgSql->createCommand()->update(self::PGSQL_TICKET, $updateParams, ['id' => $ticket['id']])->execute();
            }
        }

        $this->commit('End fixing tickets');
    }

    /**
     * Action yii import/fix-users
     */
    public function actionFixUsers()
    {
        $pgSql = Yii::$app->pgSql;

        // Remove users, who not in related tables
        if (($usersIDs = $this->getCache('active_users_ids')) === null) {
            $eventsUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(creator_id)'])
                ->from(self::PGSQL_EVENT)
                ->indexBy('id');
            $eventsUsersIDs = array_keys($eventsUsersIDsQuery->all());

            $quotasCreatorUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(creator_id)'])
                ->from(self::PGSQL_QUOTA)
                ->indexBy('id');
            $quotasCreatorUsersIDs = array_keys($quotasCreatorUsersIDsQuery->all());

            $quotasCloserUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(closer_id)'])
                ->from(self::PGSQL_QUOTA)
                ->indexBy('id');
            $quotasCloserUsersIDs = array_keys($quotasCloserUsersIDsQuery->all());

            $ordersCreatorUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(creator_id)'])
                ->from(self::PGSQL_ORDER)
                ->indexBy('id');
            $ordersCreatorUsersIDs = array_keys($ordersCreatorUsersIDsQuery->all());

            $ordersSellerUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(seller_id)'])
                ->from(self::PGSQL_ORDER)
                ->indexBy('id');
            $ordersSellerUsersIDs = array_keys($ordersSellerUsersIDsQuery->all());

            $ticketsPrinterUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(printer_id)'])
                ->from(self::PGSQL_TICKET)
                ->indexBy('id');
            $ticketsPrinterUsersIDs = array_keys($ticketsPrinterUsersIDsQuery->all());

            $ticketsSellerUsersIDsQuery = (new Query($pgSql))
                ->select(['id' => 'DISTINCT(seller_id)'])
                ->from(self::PGSQL_TICKET)
                ->indexBy('id');
            $ticketsSellerUsersIDs = array_keys($ticketsSellerUsersIDsQuery->all());

            $adminUsersIDsQuery = (new Query($pgSql))
                ->select('id')
                ->from(self::PGSQL_USER)
                ->where(['email' => ['s_vitaha@ukr.net', 'rem@mail.com', 'k.zhidkovskiy@gmail.com', 'kkliuiev@vareger.com',
                    'admin@mail.com', 'vick@gmail.com', 'oleg.pun@vareger.com', 'ypaliy@aurum-soft.com',
                    'mira.tkachenko@gmail.com', 'itsurkan@vareger.com', 'akravchuk@vareger.com', 'kasa_hall']])
                ->indexBy('id');
            $adminUsersIDs = array_keys($adminUsersIDsQuery->all());

            $usersIDs = array_unique(array_merge($eventsUsersIDs, $quotasCreatorUsersIDs, $quotasCloserUsersIDs, $ordersCreatorUsersIDs, $ordersSellerUsersIDs, $ticketsPrinterUsersIDs, $ticketsSellerUsersIDs, $adminUsersIDs));

            $this->setCache('active_users_ids', array_filter($usersIDs));
        }

        $usersIDsString = implode(',', $usersIDs);

        $this->start($pgSql, 'Fix users start');

        $pgSql->createCommand()->delete(self::PGSQL_USER, "id NOT IN({$usersIDsString})")->execute();
        $this->setSequenceValue(self::PGSQL_USER);

        $this->commit('Fix users end');
    }

    /**
     * Action yii import/fix-cities-status
     */
    public function actionFixCitiesStatus()
    {
        $pgSql = Yii::$app->pgSql;

        $citiesQuery = (new Query($pgSql))
            ->from(self::PGSQL_CITY)
            ->where(['id' => (new Query($pgSql))
                ->select('city_id')
                ->from(self::PGSQL_EVENT),
            ])
            ->indexBy('id');
        $citiesIDs = array_keys($citiesQuery->all());
        $citiesIDsString = implode(',', $citiesIDs);

        // Update cities
        $pgSql->createCommand()->update(self::PGSQL_CITY, ['is_active' => true], "id IN({$citiesIDsString})")->execute();

        echo "Cities statuses fixed";
    }

    /**
     * Action yii import/import-error-log
     */
    public function actionImportErrorLog()
    {
        $pgSql = Yii::$app->pgSql;

        // Create import log data query
        $importLogDaraQuery = (new Query($pgSql))
            ->select([
                'event_id',
                'scheme_id',
                'zone_id',
                'place_id',
                'row',
                'place',
                'type',
            ])
            ->from('import_error_log_old');

        $this->start($pgSql, 'Import error log fixing started');

        foreach ($importLogDaraQuery->each() as $log) {
            $zoneInfo = $this->getZoneInfo($log['event_id'], $log['zone_id']);
            $insertData = [
                'event_id' => $log['event_id'],
                'scheme_id' => $log['scheme_id'],
                'zone_id' => $log['zone_id'],
                'place_id' => $log['place_id'],
                'event_date' => $zoneInfo['eventDate'],
                'event_name' => $zoneInfo['eventName'],
                'location_name' => $zoneInfo['areaName'],
                'scheme_name' => $zoneInfo['hallName'],
                'zone_name' => $zoneInfo['zoneName'],
                'row' => $log['row'],
                'place' => $log['place'],
                'type' => $log['type'],
            ];
            $pgSql->createCommand()->insert(self::PGSQL_IMPORT_ERROR_LOG, $insertData)->execute();
        }

        $this->commit('Import error log fixing end');
    }

    // END Import data functions


    // Private functions

    private function getEventsIds()
    {
        if ($this->eventsIDs === null && ($this->eventsIDs = $this->getCache('events_ids')) === null) {
            $eventsIDsQuery = (new Query(Yii::$app->mySql))
                ->select('id')
                ->from(self::MYSQL_EVENT)
                ->where(['>=', 'end_sale', self::EVENT_START_DATE])
                ->indexBy('id');
            $eventsIDs = array_keys($eventsIDsQuery->all());
            if (!empty($eventsIDs)) {
                $this->setCache('events_ids', ($this->eventsIDs = $eventsIDs));
            }
        }
        return $this->eventsIDs;
    }

    /**
     * @return \app\data\Query
     */
    private function createSelectMysqlTicketsQuery($insertReturnedFromQuota = true)
    {
        $blankTypeA4 = self::TICKET_BLANK_TYPE_A4;
        $blankTypeNone = self::TICKET_BLANK_TYPE_NONE;
        $blankType5 = self::TICKET_BLANK_TYPE_A5;

        // Create ticket blank type sql
        $blankTypeSql = "CASE" .
            "  WHEN ticket.type_blank = {$blankTypeA4}" .
            "   THEN ('a4')" .
            "  WHEN ticket.type_blank = {$blankType5}" .
            "   THEN ('a5')" .
            "  WHEN ticket.type_blank = {$blankTypeNone}" .
            "   THEN ('e_blank')" .
            "  ELSE ('standard')" .
            "END";

        $adminID = $this->getAdminID();

        $ticketsQuery = (new Query(Yii::$app->mySql))
            ->select([
                'place.id',
                'ticket_id' => 'ticket.id',
                'place.event_id',
                'zone_id' => 'place.sector_id',
                'quota_id' => 'quota.id',
                'zone_name' => 'sector.name',
                'row_number' => 'place.row',
                'number' => 'place.place',
                'ticket.order_id',
                'printer_id' => 'ticket.author_print_id',
                'seller_id' => "CASE WHEN user.type = 0 THEN ticket.user_id ELSE ({$adminID}) END",
                'hall_id' => 'event.scheme_id',
                'place_hall_id' => 'sector.scheme_id',
                'price' => "(CASE WHEN ticket.price > 0 THEN ticket.price ELSE place.price END)",
                'ticket.status',
                'ticket.pay_status',
                'blank_type' => "({$blankTypeSql})",
                'print_status' => "(CASE WHEN ticket.date_print IS NOT NULL THEN ('printed') ELSE ('not_printed') END)",
                'date_print' => "(CASE WHEN ticket.date_print = '0000-00-00 00:00:00' THEN (NULL) ELSE ADDTIME(ticket.date_print, '00:00:00') END)",
                'date_sold' => "(CASE WHEN ticket.date_pay = '0000-00-00 00:00:00' THEN (NULL) ELSE ADDTIME(ticket.date_pay, '00:00:00') END)",
                'barcode' => "(CASE WHEN ticket.code IS NOT NULL AND ticket.status != :status_cancelled THEN ticket.code ELSE place.code END)",
                'discount' => "(CASE WHEN ticket.discount IS NOT NULL THEN ticket.discount ELSE 0 END)",
                'visitor_name' => 'ticket.owner_surname',
                'visitor_email' => 'ticket.owner_mail',
                'visitor_phone' => 'ticket.owner_phone',
                'payment_type' => 'ticket.pay_type',
                'payment_status' => 'ticket.pay_status',
                'ticket.delivery_type',
                'ticket.delivery_status',
                'delivery_city_id' => 'delivery.city_id',
                'delivery_address' => 'delivery.address',
                'company_id' => 'ticket.role_id',
            ])
            ->from(['place' => self::MYSQL_PLACE])
            ->where(['!=', 'place.status', 0])
            ->orderBy([
                'id' => SORT_DESC,
                'status' => SORT_DESC,
                'ticket_id' => SORT_DESC,
            ])
            ->addParams([
                'status_cancelled' => self::TICKET_STATUS_CANCELLED
            ]);

        if (!$insertReturnedFromQuota) {
            $ticketsQuery
                ->leftJoin(['ticket' => self::MYSQL_TICKET], 'ticket.place_id = place.id AND ticket.status != :returned_from_quota')
                ->addParams(['returned_from_quota' => self::TICKET_STATUS_RETURNED_FROM_QUOTA]);
        } else {
            $ticketsQuery
                ->leftJoin(['ticket' => self::MYSQL_TICKET], 'ticket.place_id = place.id');
        }

        $ticketsQuery
            ->leftJoin(['user' => self::MYSQL_USER], 'user.id = ticket.user_id')
            ->leftJoin(['sector' => self::MYSQL_ZONE], 'sector.id = place.sector_id')
            ->leftJoin(['event' => self::MYSQL_EVENT], 'event.id = place.event_id')
            ->leftJoin(['order' => self::MYSQL_ORDER], '`order`.`id` = ticket.order_id')
            ->leftJoin(['delivery' => self::MYSQL_DELIVERY], 'delivery.id = `order`.`delivery_id`')
            ->leftJoin(['quota' => self::MYSQL_QUOTA], 'quota.order_id = ticket.order_id');

        if (($eventIDs = $this->getEventsIds()) !== null) {
            $ticketsQuery->andWhere(['place.event_id' => $eventIDs]);
        }
        return $ticketsQuery;
    }

    private function writeBigData($table, $data, $errorMessage = null)
    {
        // Get DB connection instance
        $db = Yii::$app->pgSql;

        if ($errorMessage === null) {
            $errorMessage = "Can not write data to {$table}";
        }

        $limit = 1000;
        $iterationsCount = round(count($data) / $limit) + 1;

        for ($i = 0; $i < $iterationsCount; $i++) {
            $subData = array_slice($data, $i*$limit, $limit);
            if (empty($subData)) {
                break;
            }
            $insertQuery = (new Query($db))->createInsertQuery($table, $subData);
            if (!$insertQuery->execute()) {
                $this->rollback($insertQuery, $errorMessage);
            }
        }
    }

    private function getCache($fileName)
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $fileName . '.json';
        if (!file_exists($path)) {
            return null;
        }
        return json_decode(file_get_contents($path), true);
    }

    private function setCache($fileName, $data)
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . $fileName . '.json';
        file_put_contents($path, json_encode($data));
    }

    private function start(Connection $db, $message = 'Start')
    {
        if ($this->transaction === null) {
            $this->transaction = $db->beginTransaction();
        }
        echo "{$message}\n";
    }

    private function commit($message = 'All done!', $isEnd = false)
    {
        if ($this->transaction !== null) {
            $this->transaction->commit();
            $this->transaction = null;
        }
        echo "{$message}\n";
        if ($isEnd) {
            echo "<<---OK--->>\n\n";
        }
    }

    private function rollback(Query $query, $message)
    {
        if ($this->transaction !== null) {
            $this->transaction->rollBack();
        }
        $sql = substr($query->asString(), 0, 500);
        $message = substr($message, 0, 500);
        echo "Error message: {$message}\n";
        echo "Command:\n{$sql}\n";
        die;
    }

    private function writeDataFromFile($filePath, Connection $db = null, $allRequired = true)
    {
        if ($db === null) {
            $db = Yii::$app->pgSql;
        }

        // Read dump file
        $file = fopen(__DIR__ . '/data/' . $filePath, 'r');
        // Read file strings
        while (!feof($file)) {
            $string = fgets($file);
            if (!empty($string)) {
                if ($allRequired) {
                    // Execute sql string
                    $db->createCommand($string)->execute();
                } else {
                    // Try to execute sql string
                    try {
                        $db->createCommand($string)->execute();
                    } catch (\Exception $e) { }
                }
            }
        }
        // Close file
        fclose($file);
    }

    private function setSequenceValue($table, $sequence = null, Connection $db = null)
    {
        if ($sequence === null) {
            $sequence = $this->sequenceName($table);
        }

        if ($db === null) {
            $db = Yii::$app->pgSql;
        }

        $maxID = (new Query($db))->from($table)->max('"id"') + 1;

        // Set sequence value
        $db->createCommand("ALTER SEQUENCE {$sequence} RESTART WITH {$maxID}")->execute();
    }

    private function sequenceName($table)
    {
        return "{$table}_id_seq";
    }

    private function getLastInsertID($table, $db = null)
    {
        if ($db === null) {
            $db = Yii::$app->pgSql;
        }

        return $db->getLastInsertID($this->sequenceName($table));
    }

    private function getAdminID()
    {
        if ($this->_adminID === null) {
            $this->_adminID = (new Query(Yii::$app->pgSql))
                ->select('id')
                ->from(self::PGSQL_USER)
                ->where(['email' => 'kasa_hall'])
                ->limit(1)
                ->scalar();
        }
        return $this->_adminID;
    }

    private function getTicketStatus($ticket)
    {
        $status = $ticket['status'];
        $payStatus = $ticket['pay_status'];
        $quotaID = isset($ticket['quota_id']) ? $ticket['quota_id'] : null;
        //$datePrint = isset($ticket['date_print']) && $ticket['date_print'] != '0000-00-00 00:00:00' ? $ticket['date_print'] : null;

        // tbl_ticket.status == null || tbl_ticket.status == 0
        if (in_array($status, [null, self::TICKET_STATUS_CANCELLED, self::TICKET_STATUS_RETURNED_FROM_QUOTA])) {
            return 'new';
        }
        /*// tbl_ticket.status == 0
        if ($status == self::TICKET_STATUS_CANCELLED) {
            return 'returned';
        }*/
        // tbl_quote_info.id != null && ticket.status != 2 && ticket.pay_status IN(null, 0)
        if (!empty($quotaID) && $status != self::TICKET_STATUS_SOLD_IN_QUOTA && in_array($payStatus, [null, 0])) {
            return 'in_quota';
        }
        // tbl_quote_info.id != null && ticket.status == 2 && ticket.pay_status != 2
        if (!empty($quotaID) && $status == self::TICKET_STATUS_SOLD_IN_QUOTA && $payStatus != self::PAYMENT_STATUS_INVITE) {
            return 'in_partner_sales';
        }
        // tbl_ticket.pay_status == 1
        if ($payStatus == self::PAYMENT_STATUS_PAID) {
            return 'sold';
        }
        // tbl_ticket.pay_status != 1
        if ($payStatus != self::PAYMENT_STATUS_PAID) {
            return 'reserved';
        }

        return 'returned';
    }

    private function writeImportErrorLog($ticket, $type)
    {
        $zoneInfo = $this->getZoneInfo($ticket['event_id'], $ticket['zone_id']);

        $insertData = [
            'event_id' => $ticket['event_id'],
            'scheme_id' => $ticket['hall_id'],
            'zone_id' => $ticket['zone_id'],
            'place_id' => $ticket['id'],
            'event_date' => $zoneInfo['eventDate'],
            'event_name' => $zoneInfo['eventName'],
            'location_name' => $zoneInfo['areaName'],
            'scheme_name' => $zoneInfo['hallName'],
            'zone_name' => $zoneInfo['zoneName'],
            'row' => $ticket['row_number'],
            'place' => $ticket['number'],
            'type' => $type,
        ];

        Yii::$app->pgSql->createCommand()->insert(self::PGSQL_IMPORT_ERROR_LOG, $insertData)->execute();
    }

    private function getMoneyRecipientID($ticket)
    {
        $id = null;

        if ($ticket['payment_status'] == 0) {
            return $id;
        }

        switch ($ticket['payment_type']) {
            case self::PAYMENT_TYPE_CASH_PICKUP:
            case self::PAYMENT_TYPE_CASH_PICKUP_2:
                $id = $ticket['printer_id'];
                break;
            case self::PAYMENT_TYPE_CARD_PICKUP:
            case self::PAYMENT_TYPE_CARD_PICKUP_2:
                $paymentType = 'credit_card';
                break;
            case self::PAYMENT_TYPE_CARD_POST_SERVICE:
                $id = $this->getAdminID();
                break;
            default:
                $id = null;
                break;
        }

        return $id;
    }

    private function checkUserID($id)
    {
        if (empty($id)) {
            return null;
        }

        if (!isset($this->_checkedIDs['userID'])) {
            $this->_checkedIDs['userID'] = [];
        }
        if (in_array($id, $this->_checkedIDs['userID'])) {
            return $id;
        }

        $userID = (new Query(Yii::$app->pgSql))
            ->select('id')
            ->from(self::PGSQL_USER)
            ->where(['id' => $id])
            ->scalar();

        $id = empty($userID) && !$this->importUser($id) ? $this->getAdminID() : $id;
        $this->_checkedIDs['userID'][] = $id;
        return $id;
    }

    private function checkUser($id, $companyID)
    {
        if (empty($id)) {
            return null;
        }

        $key = "{$id}/{$companyID}";
        if (!isset($this->_checkedIDs['userCompanyID'])) {
            $this->_checkedIDs['userCompanyID'] = [];
        }
        if (array_key_exists($key, $this->_checkedIDs['userCompanyID'])) {
            return $this->_checkedIDs['userCompanyID'][$key];
        }

        $pgSql = Yii::$app->pgSql;

        $baseUsersQuery = (new Query($pgSql))
            ->select('*')
            ->from(self::PGSQL_USER)
            ->where(['old_id' => $id]);

        $userQuery = clone $baseUsersQuery;
        $userQuery->andWhere(['company_id' => $companyID]);

        if ($userQuery->count() > 0) {
            return $id;
        }

        $this->start($pgSql, "Import user {$key} started");

        $usersQuery = clone $baseUsersQuery;
        if (($usersCount = $usersQuery->count()) == 0) {
            $this->importUser($id);
        }

        $user = $baseUsersQuery->orderBy(['id' => SORT_ASC])->limit(1)->one();
        if (empty($user)) {
            return null;
        }

        unset($user['id']);
        $user['company_id'] = $companyID;
        if ($usersCount > 0) {
            $user['email'] = str_replace('@', "_{$usersCount}@", $user['email']);
        }

        $pgSql->createCommand()->insert(self::PGSQL_USER, $user)->execute();

        $this->commit("Import user {$key} end");

        $id = $this->getLastInsertID(self::PGSQL_USER);
        $this->_checkedIDs['userCompanyID'][$key] = $id;
        return $id;
    }

    private function importUser($id)
    {
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        $systemCompanyID = $this->getSystemCompanyID();
        $userRoleQuery = (new Query($mySql))
        ->select('user_role.role_id')
        ->from(['user_role' => self::MYSQL_USER_ROLE])
        ->where('user_role.user_id = user.id')
        ->limit(1);

        $user = (new Query($mySql))
            ->select([
                'user.id',
                'company_id' => $userRoleQuery,
                'user.email',
                'password_hash' => 'user.password',
                'user.salt',
                'access_token' => "('access_token')",
                'date_register' => 'user.reg_date',
                'first_name' => 'user.name',
                'last_name' => 'user.surname',
                'phone' => 'SUBSTRING(user.phone, 0, 24)',
                'info' => 'user.username',
                'role' => "('cashier')",
                'status' => "CASE WHEN `user`.`status` = 1 THEN ('active') ELSE ('frozen') END",
                'refresh_token' => "('refresh_token')",
                'date_token_expired' => "('01.01.2018 7:10:15')",
                'is_company_owner' => "('f')",
                'print_blank_type' => "('a4')",
            ])
            ->from(['user' => self::MYSQL_USER])
            ->where([
                'user.id' => $id,
                'user.type' => 0,
            ])
            ->one();
        if (empty($user)) {
            return null;
        }

        if (empty($user['company_id'])) {
            $user['company_id'] = $systemCompanyID;
        }
        if ($user['company_id'] != $systemCompanyID) {
            $user['company_id'] = $this->checkCompanyID($user['company_id']);
        }
        if (empty($user['email'])) {
            $user['email'] = $this->getRandomEmail($user['id']);
        }

        $user['old_id'] = $user['id'];
        $insertResult = $pgSql->createCommand()->insert(self::PGSQL_USER, $user)->execute();
        return $insertResult ? $id : null;
    }

    private function checkCompanyID($id)
    {
        if (empty($id)) {
            return null;
        }

        if (!isset($this->_checkedIDs['companyID'])) {
            $this->_checkedIDs['companyID'] = [];
        }
        if (in_array($id, $this->_checkedIDs['companyID'])) {
            return $id;
        }

        $companyID = (new Query(Yii::$app->pgSql))
            ->select('id')
            ->from(self::PGSQL_COMPANY)
            ->where(['id' => $id])
            ->scalar();

        $id = empty($companyID) && !$this->importCompany($id) ? $this->getSystemCompanyID() : $id;
        $this->_checkedIDs['companyID'][] = $id;
        return $id;
    }

    private function importCompany($id)
    {
        $pgSql = Yii::$app->pgSql;
        $mySql = Yii::$app->mySql;

        $systemCompanyID = $this->getSystemCompanyID();

        $company = (new Query($mySql))
            ->select([
                'id',
                'geo_area_id' => '(1)',
                'name',
                'description',
                'date_create' => 'date_add',
                'legal_name' => 'company_name',
                'requisites' => 'legal_detail',
                'edrpou' => 'SUBSTRING(legal_detail, 0, 14)',
                'type' => "CASE WHEN `entity` = 1 THEN 'organizer' ELSE 'ticket_agency' END",
                'status' => "('active')",
                'sc_id' => "({$systemCompanyID})",
            ])
            ->from(self::MYSQL_COMPANY)
            ->where(['id' => $id])
            ->one();
        if (empty($company)) {
            return null;
        }

        $pgSql->createCommand()->insert(self::PGSQL_COMPANY, $company)->execute();

        return $id;
    }

    private function checkOrderID($id)
    {
        if (empty($id)) {
            return null;
        }

        if (!isset($this->_checkedIDs['orderID'])) {
            $this->_checkedIDs['orderID'] = [];
        }
        if (in_array($id, $this->_checkedIDs['orderID'])) {
            return $id;
        }

        $this->_checkedIDs['orderID'][] = $id;

        $orderID = (new Query(Yii::$app->pgSql))
            ->select('id')
            ->from(self::PGSQL_ORDER)
            ->where(['id' => $id])
            ->scalar();

        return $orderID ?: null;
    }

    private function checkQuotaID($id)
    {
        if (empty($id)) {
            return null;
        }

        if (!isset($this->_checkedIDs['quotaID'])) {
            $this->_checkedIDs['quotaID'] = [];
        }
        if (in_array($id, $this->_checkedIDs['quotaID'])) {
            return $id;
        }

        $quotaID = (new Query(Yii::$app->pgSql))
            ->select('id')
            ->from(self::PGSQL_QUOTA)
            ->where(['id' => $id])
            ->scalar();

        $id = empty($quotaID) && !$this->importQuota($id) ? null : $id;
        $this->_checkedIDs['quotaID'][] = $id;
        return $id;
    }

    private function importQuota($id)
    {
        $pgSql = Yii::$app->pgSql;
        $mySql = Yii::$app->mySql;

        $creatorIDQuery = (new Query($mySql))
            ->select('creator_company.user_id')
            ->from(['creator_company' => self::MYSQL_USER_COMPANY])
            ->where('creator_company.role_id = quota.role_from_id')
            ->limit(1);

        // Quota statuses and types variables
        $typeEQuota = self::QUOTA_TYPE_E_QUOTA;

        // Create quota ticket query
        $quotaTicketSql = (new Query($mySql))
            ->select('ticket.id')
            ->from(['ticket' => self::MYSQL_TICKET])
            ->where('ticket.order_id = quota.order_id')
            ->limit(1)
            ->asString();

        // Create select quotas query
        $quota = (new Query($mySql))
            ->select([
                'quota.id',
                'quota.event_id',
                'partner_id' => 'quota.role_to_id',
                'creator_id' => $creatorIDQuery,
                'quota.name',
                'type' => "(CASE WHEN quota.type = {$typeEQuota} THEN ('e_quota') ELSE ('quota') END)",
                'status' => "(CASE WHEN ($quotaTicketSql) IS NULL THEN ('new') ELSE ('active') END)",
                'date_create' => "(CASE WHEN `order`.`date_add` = '0000-00-00 00:00:00' THEN '2016-10-01 00:00:00' ELSE ADDTIME(`order`.`date_add`, '00:00:00') END)",
                'date_close' => "(CASE WHEN `order`.`date_update` = '0000-00-00 00:00:00' THEN '2016-10-01 00:00:00' ELSE ADDTIME(`order`.`date_update`, '00:00:00') END)",
                'partner_fee_type' => "('percent')",
                'partner_fee' => 'quota.percent',
                'description' => 'quota.comment',
                'provider_id' => 'quota.role_from_id',
            ])
            ->from(['quota' => self::MYSQL_QUOTA])
            ->leftJoin(['order' => self::MYSQL_ORDER], '`order`.`id` = quota.order_id')
            ->where(['quota.id' => $id])
            ->one();
        if (empty($quota)) {
            return null;
        }

        // Check quota user
        $quota['creator_id'] = $this->checkUserID($quota['creator_id']);
        $quota['partner_id'] = empty($quota['partner_id']) && !$this->checkCompanyID($quota['partner_id']) ? $this->getSystemCompanyID() : $quota['partner_id'];
        $quota['provider_id'] = empty($quota['provider_id']) && !$this->checkCompanyID($quota['provider_id']) ? $this->getSystemCompanyID() : $quota['provider_id'];

        $pgSql->createCommand()->insert(self::PGSQL_QUOTA, $quota)->execute();

        return $id;
    }

    private function getRandomEmail($id)
    {
        return "user_{$id}@mail.com";
    }

    private function getSystemCompanyID()
    {
        if ($this->_systemCompanyID === null) {
            $this->_systemCompanyID = (new Query(Yii::$app->pgSql))
                ->select('id')
                ->from(self::PGSQL_COMPANY)
                ->where(['type' => 'system_client'])
                ->limit(1)
                ->scalar();
        }
        return $this->_systemCompanyID;
    }

    private function formatDateTime($dateTime)
    {
        return (new \DateTime($dateTime))->format('Y-m-d H:i:s');
    }

    private function generatePasswordHash($password, $email, $salt)
    {
        $salt = $email . $salt . ':' . 'UKoi&*6TY:%RT';
        $string = Yii::$app->getSecurity()->pbkdf2('sha512', $password, $salt, 150, 0);
        return hash('sha256', $string . $salt);
    }

    private function getZoneInfo($eventID, $zoneID)
    {
        $key = "{$eventID}-{$zoneID}";

        if (isset($this->_zonesInfo[$key])) {
            return $this->_zonesInfo[$key];
        }

        if (($info = $this->getCache('zones/zone_info_' . $key)) === null) {
            $info = (new Query(Yii::$app->mySql))
                ->select([
                    'eventName' => "CONCAT(event.name, ', ', city.name, ' (', DATE_FORMAT(event.start_sale, '%Y-%m-%d'), ')')",
                    'eventDate' => "DATE_FORMAT(event.start_sale, '%Y-%m-%d')",
                    'areaName' => 'area.name',
                    'hallName' => 'hall.name',
                    'zoneName' => 'zone.name',
                ])
                ->from(['zone' => self::MYSQL_ZONE])
                ->leftJoin(['event' => self::MYSQL_EVENT], 'event.id = :event_id')
                ->leftJoin(['hall' => self::MYSQL_SCHEME] , 'hall.id = zone.scheme_id')
                ->leftJoin(['area' => self::MYSQL_AREA], 'area.id = hall.location_id')
                ->leftJoin(['city' => self::MYSQL_CITY], 'city.id = area.city_id')
                ->where(['zone.id' => $zoneID])
                ->addParams(['event_id' => $eventID])
                ->one();

            $this->setCache('zones/zone_info_' . $key, $info);
        }
        return $this->_zonesInfo[$key] = $info;
    }

    // END Private functions
}