<?php
/**
 * Created by PhpStorm.
 * User: Rem
 * Date: 09.08.2016
 * Time: 12:07
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\data\Query;
use yii\db\Transaction;
use yii\db\Connection;

class TransferController extends Controller
{
    const MYSQL_COUNTRY = 'tbl_country';
    const MYSQL_REGION = 'tbl_region';
    const MYSQL_CITY = 'tbl_city';
    const MYSQL_ADDRESS = 'tbl_location';
    const MYSQL_AREA = 'tbl_location';
    const MYSQL_COMPANY = 'tbl_role';
    const MYSQL_USER = 'tbl_user';
    const MYSQL_EVENT = 'tbl_event';
    const MYSQL_EVENT_TIMING = 'tbl_timing';
    const MYSQL_SCHEME = 'tbl_scheme';
    const MYSQL_MEDIA = 'tbl_multimedia';
    const MYSQL_PLACE = 'tbl_place';

    const PGSQL_COUNTRY = 'geo_country';
    const PGSQL_GEO_AREA = 'geo_area';
    const PGSQL_REGION = 'geo_region';
    const PGSQL_CITY = 'geo_city';
    const PGSQL_ADDRESS = 'geo_address';
    const PGSQL_AREA = 'area';
    const PGSQL_HALL = 'area_file';
    const PGSQL_ZONE = 'area_zone';
    const PGSQL_PLACE = 'area_place';
    const PGSQL_COMPANY = 'company';
    const PGSQL_USER = 'user';
    const PGSQL_USER_POSITION = 'user_position';
    const PGSQL_CLIENT = 'client';
    const PGSQL_CASH = 'cash';
    const PGSQL_EVENT = 'event';
    const PGSQL_MEDIA = 'media';
    const PGSQL_TICKET = 'ticket';

    const EVENTS_FIRST_DATE = '01-01-2015';

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * Command "yii transfer/geo"
     */
    public function actionGeo()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select countries query
        $countriesQuery = (new Query($mySql))
            ->select([
                'id',
                'name',
            ])
            ->from(self::MYSQL_COUNTRY);
        // Get countries list
        if (empty($countries = $countriesQuery->all())) {
            $this->rollback($countriesQuery, "Countries not found");
        }

        // Create select regions query
        $regionsQuery = (new Query($mySql))
            ->select([
                'id',
                'area_id' => "(1)",
                'name',
            ])
            ->from(self::MYSQL_REGION);
        // Get regions list
        if (empty($regions = $regionsQuery->all())) {
            $this->rollback($regionsQuery, "Regions not found");
        }

        // Create select cities query
        $citiesQuery = (new Query($mySql))
            ->select([
                'id',
                'region_id',
                'area_id' => "(1)",
                'name',
            ])
            ->from(self::MYSQL_CITY);
        // Get countries list
        if (empty($cities = $citiesQuery->all())) {
            $this->rollback($citiesQuery, "Cities not found");
        }

        // Create select addresses query
        $addressesQuery = (new Query($mySql))
            ->select([
                'id',
                'city_id',
                'address',
                'latitude' => 'lat',
                'longitude' => 'lng',
            ])
            ->from(self::MYSQL_ADDRESS);
        // Get countries list
        if (empty($addresses = $addressesQuery->all())) {
            $this->rollback($addressesQuery, "Addresses not found");
        }

        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer geo data started');

        // Create insert countries query
        $insertCountriesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_COUNTRY, $countries);
        // Try to insert countries
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the countries");
        }

        // Set set sequence value - max ID from table "geo_country"
        $this->setSequenceValue(self::PGSQL_COUNTRY);

        // Create data for test area record
        $areaData = [[
            'id' => 1,
            'country_id' => 1,
            'name' => 'Test area',
        ]];
        // Create insert test area record
        $insertAreaQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_GEO_AREA, $areaData);
        // Try to insert test area
        if (!$insertAreaQuery->execute()) {
            $this->rollback($insertAreaQuery, 'Can not insert test area');
        }

        // Set set sequence value - max ID from table "geo_area"
        $this->setSequenceValue(self::PGSQL_GEO_AREA);

        // Create insert regions query
        $insertRegionsQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_REGION, $regions);
        // Try to insert regions
        if (!$insertRegionsQuery->execute()) {
            $this->rollback($insertRegionsQuery, 'Can not insert regions');
        }

        // Set set sequence value - max ID from table "geo_region"
        $this->setSequenceValue(self::PGSQL_REGION);

        // Create insert cities query
        $insertCitiesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_CITY, $cities);
        // Try to insert cities
        if (!$insertCitiesQuery->execute()) {
            $this->rollback($insertCitiesQuery, 'Can not insert cities');
        }

        // Set set sequence value - max ID from table "geo_city"
        $this->setSequenceValue(self::PGSQL_CITY);

        // Create insert addresses query
        $insertAddressesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_ADDRESS, $addresses);
        // Try to insert addresses
        if (!$insertAddressesQuery->execute()) {
            $this->rollback($insertAddressesQuery, 'Can not insert addresses');
        }

        // Set set sequence value - max ID from table "geo_address"
        $this->setSequenceValue(self::PGSQL_ADDRESS);

        $this->commit('Transfer geo data is done');
    }

    /**
     * Command "yii transfer/area"
     */
    public function actionArea()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        // Begin transaction
        $this->start($pgSql, 'Transfer area data started');

        // Write areas data from sql file
        $this->writeDataFromFile('area_zones_places_data.sql');

        /** Updating halls IDs in new DB (set old IDs) */

        // Find new halls
        $newHalsQuery = (new Query($pgSql))
            ->select('id')
            ->from(self::PGSQL_HALL);
        if (empty($newHalls = $newHalsQuery->all())) {
            $this->rollback($newHalsQuery, 'New halls area nof found');
        }
        // Get max ID form new halls
        $newHallsMaxID = (new Query($pgSql))->from(self::PGSQL_HALL)->max('id');
        $newHallsID = $newHallsMaxID + 100;
        // Update new halls - set new IDs
        foreach ($newHalls as $newHall) {
            $pgSql->createCommand()->update(self::PGSQL_HALL, ['id' => ++$newHallsID], ['id' => $newHall['id']])->execute();
        }

        // Get old halls
        $oldHallsQuery = (new Query($mySql))
            ->select([
                'hall.id',
                'hall.name',
                'areaID' => 'hall.location_id',
                'areaName' => 'area.name',
            ])
            ->from(['hall' => self::MYSQL_SCHEME])
            ->leftJoin(['area' => self::MYSQL_AREA], 'area.id = hall.location_id');
        if (empty($oldHalls = $oldHallsQuery->all())) {
            $this->rollback($oldHallsQuery, 'Old halls are not found');
        }
        // Find the hall in new db - by name and hall ID and update this ID
        // Create query for searching hall in new DB
        $newHallQuery = (new Query($pgSql))
            ->select('hall.id')
            ->from(['hall' => self::PGSQL_HALL])
            ->leftJoin(['area' => self::PGSQL_AREA], 'area.id = hall.area_id');
        foreach ($oldHalls as $hall) {
            // Add search condition to search new hall query
            $newHallQuery
                ->where('(hall.area_id = :area_id AND LOWER(hall.name) LIKE LOWER(:hall_name)) OR (LOWER(area.name) LIKE LOWER(:area_name) AND LOWER(hall.name) LIKE LOWER(:hall_name))')
                ->params([
                    ':area_id' => $hall['areaID'],
                    ':hall_name' => "%{$hall['name']}%",
                    ':area_name' => "%{$hall['areaName']}%",
                ]);
            // Try to find new hall. If its not found or has old ID - skip this iteration
            if (empty($newHall = $newHallQuery->one()) || $newHall['id'] == $hall['id']) {
                continue;
            }

            // Update new hall - set for him old ID
            $pgSql->createCommand()->update(self::PGSQL_HALL, ['id' => $hall['id']], ['id' => $newHall['id']])->execute();
        }

        // Set set sequence value - max ID from table "area"
        $this->setSequenceValue(self::PGSQL_AREA);
        // Set set sequence value - max ID from table "area_file"
        $this->setSequenceValue(self::PGSQL_HALL);
        // Set set sequence value - max ID from table "area_zone"
        $this->setSequenceValue(self::PGSQL_ZONE);
        // Set set sequence value - max ID from table "area_place"
        $this->setSequenceValue(self::PGSQL_PLACE);

        $this->commit('Transfer area data is done');
    }

    /**
     * Command "yii transfer/company"
     */
    public function actionCompany()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select companies query
        $companiesQuery = (new Query($mySql))
            ->select([
                'id',
                'geo_area_id' => '(1)',
                'name',
                'description',
                'date_create' => 'date_add',
                'legal_name' => 'company_name',
                'requisites' => 'legal_detail',
                'edrpou' => 'SUBSTRING(legal_detail, 0, 14)',
                'type' => "CASE WHEN `entity` = 1 THEN 'organizer' ELSE 'ticket_agency' END",
                'status' => "('active')",
                'sc_id' => '(11)',
            ])
            ->from(self::MYSQL_COMPANY)
            ->where(['IS NOT', 'name', NULL])
            ->andWhere(['<>', 'id', 1]);


        // Get companies list
        if (empty($companies = $companiesQuery->all())) {
            $this->rollback($companiesQuery, "Countries not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer companies data started');

        // Create insert companies query
        $insertCountriesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_COMPANY, $companies);
        // Try to insert companies
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the companies");
        }

        // Set "system_client" type to kasa.in.ua
        if (!$pgSql->createCommand()->update(self::PGSQL_COMPANY, ['type' => 'system_client', 'sc_id' => null], ['id' => 11])->execute()) {
            $this->rollback($insertCountriesQuery, "Can no update kasa.in.ua");
        }

        // Set set sequence value - max ID from table "company"
        $this->setSequenceValue(self::PGSQL_COMPANY);

        $this->commit('Transfer companies data is done');
    }

    /**
     * Command "yii transfer/user"
     */
    public function actionUser()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select users query
        $usersQuery = (new Query($mySql))
            ->select([
                'id',
                'company_id' => '(11)',
                'email',
                'password_hash' => 'password',
                'salt',
                'access_token' => "('access_token')",
                'date_register' => 'reg_date',
                'first_name' => 'name',
                'last_name' => 'surname',
                'phone' => 'SUBSTRING(phone, 0, 24)',
                'info' => 'username',
                'role' => "('user')",
                'status' => "CASE WHEN `status` = 1 THEN 'active' ELSE 'frozen' END",
                'refresh_token' => "('refresh_token')",
                'date_token_expired' => "('01.01.2017 7:10:15')",
                'is_company_owner' => "('f')",
                'print_blank_type' => "('a4')",
            ])
            ->from(self::MYSQL_USER)
            ->where(['type' => 0])
            ->andWhere(['<>', 'id', 1]);
        // Get users list
        if (empty($users = $usersQuery->all())) {
            $this->rollback($usersQuery, "Users not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer users data started');

        // Write user positions data from sql file
        $this->writeDataFromFile('positions_data.sql');

        // Set set sequence value - max ID from table "user_position"
        $this->setSequenceValue(self::PGSQL_USER_POSITION);

        // Create insert users query
        $insertCountriesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_USER, $users);
        // Try to insert users
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the users");
        }

        // Set set sequence value - max ID from table "user"
        $this->setSequenceValue(self::PGSQL_USER);

        // Write users data from sql file
        $this->writeDataFromFile('users_data.sql');

        $this->commit('Transfer users data is done');
    }

    /**
     * Command "yii transfer/client"
     */
    public function actionClient()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select clients query
        $clientsQuery = (new Query($mySql))
            ->select([
                'id',
                'sc_id' => "(11)",
                'email',
                'phone' => 'SUBSTRING(phone, 0, 24)',
                'status' => "CASE WHEN `status` = 1 THEN 'unregistered' ELSE 'disabled' END",
                'date_create' => 'reg_date',
                'first_name' => 'name',
                'last_name' => 'surname',
            ])
            ->from(self::MYSQL_USER)
            ->where(['type' => 1]);
        // Get clients list
        if (empty($clients = $clientsQuery->all())) {
            $this->rollback($clientsQuery, "Clients not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer clients data started');

        // Create insert users query
        $insertCountriesQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_CLIENT, $clients);
        // Try to insert users
        if (!$insertCountriesQuery->execute()) {
            $this->rollback($insertCountriesQuery, "Can no insert the clients");
        }

        // Set set sequence value - max ID from table "client"
        $this->setSequenceValue(self::PGSQL_CLIENT);

        $this->commit('Transfer clients data is done');
    }

    /**
     * Command "yii transfer/cash"
     */
    public function actionCash()
    {
        // Get PgSQL Db connection instance
        $pgSql = Yii::$app->pgSql;

        // Begin transaction
        $this->start($pgSql, 'Transfer cashes data started');

        // Write cashes data from sql file
        $this->writeDataFromFile('cashes_data.sql');

        // Set set sequence value - max ID from table "geo_address"
        $this->setSequenceValue(self::PGSQL_ADDRESS);

        // Set set sequence value - max ID from table "cash"
        $this->setSequenceValue(self::PGSQL_CASH);

        $this->commit('Transfer cashes data is done');
    }

    /**
     * Command "yii transfer/event"
     */
    public function actionEvent()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Get new DB halls IDs
        $hallsIDsQuery = (new Query($pgSql))
            ->select('id')
            ->from(self::PGSQL_HALL);
        $hallsIDs = array_map(function ($hall) { return $hall['id']; }, $hallsIDsQuery->all());

        // Find halls IDs in events
        $eventsHallsIDsQuery = (new Query($mySql))
            ->select('scheme_id')
            ->from(self::MYSQL_EVENT)
            ->where(['>=', 'date_add', $this->formatDateTime(self::EVENTS_FIRST_DATE)]);

        // Find halls that do not exist in the new db
        $oldHalls = (new Query($mySql))
            ->select([
                'id',
                'name',
                'area_id' => 'location_id',
                'type' => "('hall')",
                'status' => "('active')",
            ])
            ->from(self::MYSQL_SCHEME)
            ->where(['id' => $eventsHallsIDsQuery])
            ->andWhere(['NOT IN', 'id', $hallsIDs])
            ->all();
        foreach ($oldHalls as $oldHall) {
            // Add new hall to postgres DB
            $pgSql->createCommand()->insert(self::PGSQL_HALL, $oldHall)->execute();
        }

        // Create select events query
        $eventsQuery = (new Query($mySql))
            ->select([
                'event.id',
                'city_id' => 'area.city_id',
                'area_file_id' => 'event.scheme_id',
                'organizer_id' => 'event.role_id',
                'creator_id' => 'event.user_id',
                'event.name',
                'type' => "('concert')",
                'status' => "CASE WHEN `event`.`status` = 0 THEN 'new' WHEN `event`.`status` = 1 THEN 'in_sales' ELSE 'deleted' END",
                'barcode_type' => "CASE WHEN `event`.`barcode_type` = 0 THEN 'code128' ELSE 'ean13' END",
                'date_create' => 'event.date_add',
                'date_sales_start' => "`event`.`start_sale`",
                'date_sales_end' => "`event`.`end_sale`",
                'date_entry' => "CASE WHEN `timing`.`entrance` IS NULL THEN `event`.`start_sale` ELSE `timing`.`entrance` END",
                'date_start' => "CASE WHEN `timing`.`start_sale` IS NULL THEN `event`.`start_sale` ELSE `timing`.`start_sale` END",
                'date_end' => "CASE WHEN `timing`.`stop_sale` IS NULL THEN `event`.`end_sale` ELSE `timing`.`stop_sale` END",
                'description' => 'event.description_id',
                'url_alias' => 'event.url',
                'html_caption' => 'event.html_header',
                'event.keywords',
                'event.meta_description',
                'system_name' => 'event.sys_name',
                'in_main_slider' => "CASE WHEN `event`.`slider_main` = 0 THEN ('f') ELSE ('t') END",
                'in_city_slider' => "CASE WHEN `event`.`slider_city` = 0 THEN ('f') ELSE ('t') END",
                'in_main_page' => "('f')",
            ])
            ->from(['event' => self::MYSQL_EVENT])
            -> leftJoin(['timing' => self::MYSQL_EVENT_TIMING], 'timing.event_id = event.id AND timing.status = 1')
            -> leftJoin(['scheme' => self::MYSQL_SCHEME], 'scheme.id = event.scheme_id')
            -> leftJoin(['area' => self::MYSQL_AREA], 'area.id = scheme.location_id')
            -> where(['>=', 'event.date_add', $this->formatDateTime(self::EVENTS_FIRST_DATE)])
            -> where(['IN', 'event.id', [992,1134,1144,1167,1214,1277,1299,1168,1333,1327,1066,1067,1068,1055,973,1119,1120,1121,1227,1328,1329,1330,1392,1393,1395,1396,1057,1228,1139,1423,1421,994,1379,1013,1027,1028,1223,1031,1202,1024,1059,1171,1037,1056,1060,1101,1102,1103,1104,1125,1146,1169,1191,1216,1231,1260,1259,1261,1262,1263,1264,1265,1266,1267,1268,1269,1270,1271,1272,1273,1274,1275,1319,1320,1324,1336,1340,1342,1355,1357,1356,1372,1373,1374,1375,1386,1394,1405,1414]]);

        // If halls table in new DB is not empty - add "scheme_id" condition to events search query
        if (!empty($hallsIDs)) {
            $eventsQuery->andWhere(['event.scheme_id' => $hallsIDs]);
        }

        //echo '<pre>'; print_r(count($eventsQuery->all())); exit('</pre>');

        // Get clients list
        if (empty($events = $eventsQuery->all())) {
            $this->rollback($eventsQuery, "Events not found");
        }

        //, MYSQL_AREA

        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer events data started');

        // Create insert users query
        $insertEventsQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_EVENT, $events);
        // Try to insert users
        if (!$insertEventsQuery->execute()) {
            $this->rollback($insertEventsQuery, "Can no insert the events");
        }

        // Set set sequence value - max ID from table "client"
        $this->setSequenceValue(self::PGSQL_EVENT);

        $this->commit('Transfer events data is done');
    }

    /**
     * Command "yii transfer/media"
     */
    public function actionMedia()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select media query
        $mediaQuery = (new Query($mySql))
            ->select([
                'id',
                'entity_id' => 'event_id'  ,
                'entity_class' => "('models\\Event')",
                'path'=>'file',
                'type'=>"('image')",
                'status' => "CASE WHEN `status` = 0 THEN 'main' ELSE 'media' END",

                ])
            ->where(['IS NOT', 'event_id', null])
            ->from(self::MYSQL_MEDIA);

        // Get media list
        if (empty($media = $mediaQuery->all())) {
            $this->rollback($mediaQuery, "Media not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer media data started');

        // Create insert media query
        $insertMediaQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_MEDIA, $media);
        // Try to insert media
        if (!$insertMediaQuery->execute()) {
            $this->rollback($insertMediaQuery, "Can no insert the media");
        }

        // Set set sequence value - max ID from table "media"
        $this->setSequenceValue(self::PGSQL_MEDIA);

        $this->commit('Transfer media data is done');
    }


    /**
     * Command "yii transfer/AreaAdd"
     */
    public function actionAreaadd()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select companies query
        $areaaddQuery = (new Query($mySql))
            ->select([
                'id',
                'address_id'=>'id',
                'name',
                'type' => "CASE `location_category_id`  
                 WHEN  6 THEN 'stadium'
                 WHEN  8 THEN 'concert_hall'
                 WHEN  3 THEN 'club'
                 ELSE 'stadium' END",
                'description' =>'sys_name',
                'date_create' => "('2015-01-01')",
            ])
            ->from(self::MYSQL_AREA)
            ->where(['NOT IN', 'id', [1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,72,73,74,75,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,212,213,214,215,216,217,218,219,220,222,223,224,225,226,227,228,229,230,231,232,233,234]]);
        // Get areaadd list
        if (empty($areaadd = $areaaddQuery->all())) {
            $this->rollback($areaaddQuery, "Areaadd not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer areaadd data started');

        // Create insert companies query
        $insertAreaaddQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_AREA, $areaadd);
        // Try to insert areaadd
        if (!$insertAreaaddQuery->execute()) {
            $this->rollback($insertAreaaddQuery, "Can no insert the areas");
        }

        // Set set sequence value - max ID from table "area"
        $this->setSequenceValue(self::PGSQL_AREA);

        $this->commit('Transfer areaadd data is done');
    }



    /**
     * Command "yii transfer/AreaFileAdd"
     */
    public function actionAreafileadd()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select companies query
        $areafileaddQuery = (new Query($mySql))
            ->select([
                'id',
                'area_id'=>'location_id',
                'name',
                'type' =>"('hall')",
                'status' =>"('active')",
                'description' =>'name',

            ])
            ->from(self::MYSQL_SCHEME)
            ->where(['NOT IN', 'id', [83,218,21,1,2,3,7,8,9,10,12,13,14,15,16,17,19,20,22,23,24,25,26,27,28,30,31,32,33,34,35,37,38,39,40,41,44,45,46,48,49,50,51,52,53,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,84,85,86,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,163,165,166,168,170,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,191,193,194,195,196,197,198,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,219,220,222,223,224,225,226,227,228,230,232,233,238,241,242,243,244,245,246,247,248,249,250,251,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,298,299,300,301,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,361,366,399]]);
        // Get areafileadd list
        if (empty($areafileadd = $areafileaddQuery->all())) {
            $this->rollback($areafileaddQuery, "AreaFileadd not found");
        }


        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer areafileadd data started');

        // Create insert companies query
        $insertAreafileaddQuery = (new Query($pgSql))->createInsertQuery(self::PGSQL_HALL, $areafileadd);
        // Try to insert areafileadd
        if (!$insertAreafileaddQuery->execute()) {
            $this->rollback($insertAreafileaddQuery, "Can no insert the areas");
        }

        // Set set sequence value - max ID from table "area"
        $this->setSequenceValue(self::PGSQL_HALL);

        $this->commit('Transfer areaFILEadd data is done');
    }

    /**
     * Command "yii transfer/Place"
     */
    public function actionPlace()
    {
        // Get MySQL and PgSQL Db connections instances
        $mySql = Yii::$app->mySql;
        $pgSql = Yii::$app->pgSql;

        /** Get data */

        // Create select companies query
        $placeQuery = (new Query($mySql))
            ->select([
                'event_id',
                'zone_id'=> 'sector_id',
                'place_id',
                'price',
                'type' =>"('ticket')",
                'status' =>"('new')",
                'blank_type' =>"('standard')",
                'print_status' =>"('not_printed')",
                'barcode'=>'code',

            ])
            ->from(self::MYSQL_PLACE)
            ->where([
                'type' => 1,
                'status' => 0,
            ]);
            

        // Get place list
        if (empty($places = $placeQuery->all())) {
            $this->rollback($placeQuery, "Place not found");
        }
        foreach ($places as $index => $place) {
            $zone=(new Query($pgSql))->select("id")->where(['front_id'=>$place["zone_id"]])->one();
            if (empty($zone)) {
                continue;
            };
            $places[$index]['zone_id'] = $zone['id'];


        }

        /** Write data */

        // Begin transaction
        $this->start($pgSql, 'Transfer places data started');

        // Create insert companies query

        // Try to insert place


        $this->commit('Transfer place data is done');
    }




    public function actionAll()
    {
        $this->actionGeo();
        $this->actionArea();
        //$this->actionAreaadd();
        //$this->actionAreafileadd();
        $this->actionCompany();
        $this->actionUser();
        $this->actionClient();
        $this->actionCash();
        $this->actionEvent();
        $this->actionMedia();
    }

    private function start(Connection $db, $message = 'Start')
    {
        $this->transaction = $db->beginTransaction();
        echo "{$message}\n";
    }

    private function commit($message = 'All done!')
    {
        if ($this->transaction !== null) {
            $this->transaction->commit();
        }
        echo "{$message}\n\n";
    }

    private function rollback(Query $query, $message)
    {
        if ($this->transaction !== null) {
            $this->transaction->rollBack();
        }
        echo "Error message: {$message}\n";
        echo "Command:\n{$query->asString()}\n";
        die;
    }

    private function writeDataFromFile($filePath, Connection $db = null, $allRequired = true)
    {
        if ($db === null) {
            $db = Yii::$app->pgSql;
        }

        // Read dump file
        $file = fopen(__DIR__ . '/data/' . $filePath, 'r');
        // Read file strings
        while (!feof($file)) {
            $string = fgets($file);
            if (!empty($string)) {
                if ($allRequired) {
                    // Execute sql string
                    $db->createCommand($string)->execute();
                } else {
                    // Try to execute sql string
                    try {
                        $db->createCommand($string)->execute();
                    } catch (\Exception $e) { }
                }
            }
        }
        // Close file
        fclose($file);
    }

    private function setSequenceValue($table, $sequence = null, Connection $db = null)
    {
        if ($sequence === null) {
            $sequence = "{$table}_id_seq";
        }

        if ($db === null) {
            $db = Yii::$app->pgSql;
        }

        $maxID = (new Query($db))->from($table)->max('"id"') + 1;

        // Set sequence value
        $db->createCommand("ALTER SEQUENCE {$sequence} RESTART WITH {$maxID}")->execute();
    }

    private function formatDateTime($dateTime)
    {
        return (new \DateTime($dateTime))->format('Y-m-d H:i:s');
    }
}