<?php

use yii\db\Migration;

class m160822_201203_install_tables extends Migration
{
    public function up()
    {
        $transaction = Yii::$app->db->beginTransaction();

        // ENUM TYPES
        $this->execute('CREATE TYPE company_type_enum AS ENUM (\'organizer\',\'ticket_agency\',\'individual\',\'cash\',\'system_client\')');
        $this->execute('CREATE TYPE company_status_enum AS ENUM (\'active\',\'disabled\',\'deleted\')');
        $this->execute('CREATE TYPE user_role_enum AS ENUM (\'user\',\'admin\',\'call_centre\',\'cashier\',\'order_manager\',\'organizer_manager\',\'read_organizer\',\'financier\',\'technical_support\',\'read_admin\',\'partner\')');
        $this->execute('CREATE TYPE user_status_enum AS ENUM (\'active\',\'frozen\',\'banned\',\'deleted\')');
        $this->execute('CREATE TYPE area_type_enum AS ENUM (\'stadium\',\'concert_hall\',\'restaurant\',\'club\',\'hangar\')');
        $this->execute('CREATE TYPE area_zone_type_enum AS ENUM (\'sector\',\'fan\',\'scene\',\'table\',\'room\',\'balcony\',\'vip\',\'stairs\',\'hall\',\'sky_box\')');
        $this->execute('CREATE TYPE area_zone_status_enum AS ENUM (\'active\',\'not_active\',\'hidden\')');
        $this->execute('CREATE TYPE area_file_status_enum AS ENUM (\'active\',\'not_active\')');
        $this->execute('CREATE TYPE area_file_type_enum AS ENUM (\'stadium\',\'hall\')');
        $this->execute('CREATE TYPE area_place_type_enum AS ENUM (\'place\',\'by_request\')');
        $this->execute('CREATE TYPE event_type_enum AS ENUM (\'concert\',\'match\',\'theater\',\'circus\',\'recital\',\'training\',\'workshop\',\'standup\',\'lecture\',\'forum\',\'auction\',\'party\',\'performance\',\'film\',\'opera\',\'ballet\',\'dance_show\')');
        $this->execute('CREATE TYPE event_status_enum AS ENUM (\'new\',\'in_sales\',\'out_of_sales\',\'in_process\',\'done\',\'archived\',\'deleted\',\'canceled\',\'on_hold\')');
        $this->execute('CREATE TYPE event_category_enum AS ENUM (\'concert\',\'theater\',\'sport\',\'children\')');
        $this->execute('CREATE TYPE barcode_type_enum AS ENUM (\'ean8\',\'ean13\',\'upc\',\'std25\',\'int25\',\'code11\',\'code39\',\'code93\',\'code128\',\'codabar\',\'msi\',\'datamatrix\')');
        $this->execute('CREATE TYPE delivery_type_enum AS ENUM (\'courier\',\'pickup\',\'post_service\',\'internet\',\'post\')');
        $this->execute('CREATE TYPE delivery_status_enum AS ENUM (\'future\',\'current\',\'past\',\'canceled\',\'failed\',\'returned\')');
        $this->execute('CREATE TYPE media_type_enum AS ENUM (\'file\',\'image\',\'url\',\'video\',\'sound\',\'widget\',\'iframe\')');
        $this->execute('CREATE TYPE media_status_enum AS ENUM (\'main\',\'media\',\'disabled\')');
        $this->execute('CREATE TYPE order_type_enum AS ENUM (\'order\',\'reserve\',\'e_order\',\'invite\',\'present\',\'partner_sales\')');
        $this->execute('CREATE TYPE order_status_enum AS ENUM (\'new\',\'current\',\'past\',\'canceled\',\'failed\',\'return\',\'archived\',\'deleted\')');
        $this->execute('CREATE TYPE ticket_type_enum AS ENUM (\'ticket\',\'e_ticket\',\'present\',\'by_request\',\'invite\')');
        $this->execute('CREATE TYPE ticket_status_enum AS ENUM (\'new\',\'in_quota\',\'in_sales\',\'reserved\',\'sold\',\'hidden\',\'failed\',\'returned\',\'archived\',\'deleted\')');
        $this->execute('CREATE TYPE ticket_blank_type_enum AS ENUM (\'standard\',\'e_blank\',\'a4\',\'a5\',\'all\')');
        $this->execute('CREATE TYPE ticket_print_status_enum AS ENUM (\'not_printed\',\'printed\')');
        $this->execute('CREATE TYPE quota_type_enum AS ENUM (\'quota\',\'single_space\')');
        $this->execute('CREATE TYPE quota_status_enum AS ENUM (\'new\',\'active\',\'returned\',\'canceled\',\'deleted\')');
        $this->execute('CREATE TYPE payment_type_enum AS ENUM (\'cash\',\'credit_card\',\'cash_on_delivery\',\'cashless\',\'bank_transfer\',\'payment_service\')');
        $this->execute('CREATE TYPE payment_status_enum AS ENUM (\'not_paid\',\'paid\',\'canceled\',\'failed\',\'invite\')');
        $this->execute('CREATE TYPE report_type_enum AS ENUM (\'quota\',\'tickets\',\'kg-10\',\'kg-9\',\'barcodes\',\'hall_prices\',\'hall_zones\')');
        $this->execute('CREATE TYPE report_ext_enum AS ENUM (\'xls\',\'pdf\',\'csv\')');
        $this->execute('CREATE TYPE story_ticket_action_enum AS ENUM(\'updated\',\'created\',\'set_price\',\'hide\',\'add_to_quota\',\'reserved\',\'sold\',\'returned\',\'canceled\',\'failed\',\'deleted\',\'archived\',\'printed\',\'set_discount\')');
        $this->execute('CREATE TYPE partner_fee_type_enum AS ENUM(\'percent\',\'fee\')');
        $this->execute('CREATE TYPE client_status_enum AS ENUM (\'unregistered\',\'not_confirmed\',\'enabled\',\'disabled\',\'deleted\')');
        $this->execute('CREATE TYPE news_status_enum AS ENUM (\'active\',\'not_active\')');
        $this->execute('CREATE TYPE slider_status_enum AS ENUM (\'active\',\'not_active\')');
        $this->execute('CREATE TYPE cash_status_enum AS ENUM (\'active\',\'not_active\')');

        // TABLES

        // geo_country
        $this->execute('CREATE TABLE geo_country
            (
              id bigserial NOT NULL,
              name character varying(255) NOT NULL,
              CONSTRAINT geo_country_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE geo_country OWNER TO adempiere');
        // geo_area
        $this->execute('CREATE TABLE geo_area
            (
              id bigserial NOT NULL,
              country_id bigint NOT NULL,
              name character varying NOT NULL,
              CONSTRAINT geo_area_pkey PRIMARY KEY (id),
              CONSTRAINT geo_area_country_id_fkey FOREIGN KEY (country_id)
                  REFERENCES geo_country (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE geo_area OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_geo_area_county_id ON geo_area USING btree (country_id)');
        // geo_region
        $this->execute('CREATE TABLE geo_region
            (
              id bigserial NOT NULL,
              area_id bigint NOT NULL,
              name character varying(255) NOT NULL,
              CONSTRAINT geo_region_pkey PRIMARY KEY (id),
              CONSTRAINT geo_region_area_id_fkey FOREIGN KEY (area_id)
                  REFERENCES geo_area (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE geo_region OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_geo_region_area_id ON geo_region USING btree (area_id)');
        // geo_city
        $this->execute('CREATE TABLE geo_city
            (
              id bigserial NOT NULL,
              region_id bigint NOT NULL,
              area_id bigint,
              name character varying(255) NOT NULL,
              CONSTRAINT geo_city_pkey PRIMARY KEY (id),
              CONSTRAINT geo_city_area_id_fkey FOREIGN KEY (area_id)
                  REFERENCES geo_area (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT geo_city_region_id_fkey FOREIGN KEY (region_id)
                  REFERENCES geo_region (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE geo_city OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_geo_city_area_id ON geo_city USING btree (area_id)');
        $this->execute('CREATE INDEX fki_geo_city_region_id ON geo_city USING btree (region_id)');
        // geo_address
        $this->execute('CREATE TABLE geo_address
            (
              id bigserial NOT NULL,
              city_id bigint NOT NULL,
              address character varying(255),
              zip_code character varying(16),
              latitude character varying(24),
              longitude character varying(24),
              CONSTRAINT geo_address_pkey PRIMARY KEY (id),
              CONSTRAINT geo_address_city_id FOREIGN KEY (city_id)
                  REFERENCES geo_city (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE geo_address OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_geo_address_city_id ON geo_address USING btree (city_id)');
        // company
        $this->execute('CREATE TABLE company
            (
              id bigserial NOT NULL,
              geo_area_id bigint NOT NULL,
              name character varying(255) NOT NULL,
              description text,
              date_create timestamp(6) with time zone NOT NULL DEFAULT now(),
              legal_name character varying(255),
              requisites text,
              edrpou character varying(15),
              type company_type_enum NOT NULL DEFAULT \'ticket_agency\'::company_type_enum,
              status company_status_enum NOT NULL DEFAULT \'active\'::company_status_enum,
              sc_id bigint,
              address_id bigint,
              contact text,
              CONSTRAINT company_pkey PRIMARY KEY (id),
              CONSTRAINT company_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT company_geo_area_id FOREIGN KEY (geo_area_id)
                  REFERENCES geo_area (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE company OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_company_address_id ON company USING btree (address_id)');
        $this->execute('CREATE INDEX fki_company_geo_area_id ON company USING btree (geo_area_id)');
        // user_position
        $this->execute('CREATE TABLE user_position
            (
              id bigserial NOT NULL,
              name character varying(255) NOT NULL,
              CONSTRAINT user_position_pkey PRIMARY KEY (id)
            )
            WITH (
              OIDS=FALSE
            )');
        $this->execute('ALTER TABLE user_position OWNER TO adempiere');
        // user
        $this->execute('CREATE TABLE "user"
            (
              id bigserial NOT NULL,
              company_id bigint NOT NULL,
              position_id bigint,
              email character varying(255) NOT NULL,
              password_hash character varying(64) NOT NULL,
              salt character varying(25) NOT NULL,
              access_token character varying(64) NOT NULL,
              date_register timestamp without time zone NOT NULL DEFAULT now(),
              first_name character varying(255),
              last_name character varying(255),
              phone character varying(25),
              info text,
              role user_role_enum NOT NULL DEFAULT \'user\'::user_role_enum,
              status user_status_enum NOT NULL DEFAULT \'active\'::user_status_enum,
              refresh_token character varying(64) NOT NULL,
              date_token_expired timestamp without time zone NOT NULL,
              is_company_owner boolean NOT NULL DEFAULT false,
              print_blank_type ticket_blank_type_enum,
              CONSTRAINT user_pkey PRIMARY KEY (id),
              CONSTRAINT user_company_id_fkey FOREIGN KEY (company_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT user_position_id FOREIGN KEY (position_id)
                  REFERENCES user_position (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE "user" OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_user_company_id ON "user" USING btree (company_id)');
        $this->execute('CREATE INDEX fki_user_position_id ON "user" USING btree (position_id)');
        // story
        $this->execute('CREATE TABLE story
            (
              id bigserial NOT NULL,
              entity_class character varying(126) NOT NULL,
              entity_id bigint NOT NULL,
              user_id bigint,
              date timestamp without time zone NOT NULL DEFAULT now(),
              action character varying(126) NOT NULL,
              fields_json json,
              old_values_json json,
              new_values_json json,
              CONSTRAINT story_pkey PRIMARY KEY (id),
              CONSTRAINT story_user_id FOREIGN KEY (user_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE story OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_story_user_id ON story USING btree (user_id)');
        $this->execute('CREATE INDEX story_entity_class ON story USING btree (entity_class COLLATE pg_catalog."default")');
        $this->execute('CREATE INDEX story_entity_id ON story USING btree (entity_id)');
        // client
        $this->execute('CREATE TABLE client
            (
              id bigserial NOT NULL,
              sc_id bigint NOT NULL,
              address_id bigint,
              email character varying(255),
              phone character varying(25),
              password_hash character varying(64),
              status client_status_enum NOT NULL DEFAULT \'unregistered\'::client_status_enum,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              info text,
              date_update timestamp without time zone NOT NULL DEFAULT now(),
              first_name character varying(255),
              last_name character varying(255),
              middle_name character varying(255),
              password_reset_token character varying(64),
              post_office_number integer,
              CONSTRAINT client_pkey PRIMARY KEY (id),
              CONSTRAINT client_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT client_sc_id FOREIGN KEY (sc_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE client OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_client_address_id ON client USING btree (address_id)');
        $this->execute('CREATE INDEX fki_client_sc_id ON client USING btree (sc_id)');
        // area
        $this->execute('CREATE TABLE area
            (
              id bigserial NOT NULL,
              address_id bigint,
              name character varying(255) NOT NULL,
              type area_type_enum NOT NULL DEFAULT \'concert_hall\'::area_type_enum,
              phone character varying(26),
              description text,
              data_json json,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              CONSTRAINT area_pkey PRIMARY KEY (id),
              CONSTRAINT area_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE area OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_area_address_id ON area USING btree (address_id)');
        // area_file
        $this->execute('CREATE TABLE area_file
            (
              id bigserial NOT NULL,
              area_id bigint NOT NULL,
              name character varying(255),
              file_path character varying(255),
              type area_file_type_enum NOT NULL DEFAULT \'stadium\'::area_file_type_enum,
              status area_file_status_enum NOT NULL DEFAULT \'active\'::area_file_status_enum,
              description text,
              CONSTRAINT area_file_pkey PRIMARY KEY (id),
              CONSTRAINT area_file_area_id FOREIGN KEY (area_id)
                  REFERENCES area (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE area_file OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_area_file_area_id ON area_file USING btree (area_id)');
        // area_zone
        $this->execute('CREATE TABLE area_zone
            (
              id bigserial NOT NULL,
              area_file_id bigint NOT NULL,
              front_id integer NOT NULL,
              name character varying(255) NOT NULL,
              type area_zone_type_enum NOT NULL DEFAULT \'sector\'::area_zone_type_enum,
              status area_zone_status_enum NOT NULL DEFAULT \'active\'::area_zone_status_enum,
              info_json json,
              "limit" integer,
              CONSTRAINT area_zone_pkey PRIMARY KEY (id),
              CONSTRAINT area_zone_area_file_id FOREIGN KEY (area_file_id)
                  REFERENCES area_file (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE area_zone OWNER TO adempiere');
        $this->execute('CREATE INDEX area_zone_front_id ON area_zone USING btree (front_id)');
        $this->execute('CREATE INDEX fki_zone_area_file_id ON area_zone USING btree (area_file_id)');
        // area_place
        $this->execute('CREATE TABLE area_place
            (
              id bigserial NOT NULL,
              zone_id bigint NOT NULL,
              front_id integer NOT NULL,
              row_number character varying(15),
              "number" character varying(15) NOT NULL,
              type area_place_type_enum NOT NULL DEFAULT \'place\'::area_place_type_enum,
              CONSTRAINT area_place_pkey PRIMARY KEY (id),
              CONSTRAINT area_place_zone_id FOREIGN KEY (zone_id)
                  REFERENCES area_zone (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE area_place OWNER TO adempiere');
        $this->execute('CREATE INDEX area_place_front_id ON area_place USING btree (front_id)');
        $this->execute('CREATE INDEX fki_area_place_zone_id ON area_place USING btree (zone_id)');
        // executor
        $this->execute('CREATE TABLE executor
            (
              id bigserial NOT NULL,
              name character varying(255) NOT NULL,
              CONSTRAINT executor_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE executor OWNER TO adempiere');
        // event
        $this->execute('CREATE TABLE event
            (
              id bigserial NOT NULL,
              city_id bigint NOT NULL,
              area_file_id bigint NOT NULL,
              organizer_id bigint NOT NULL,
              creator_id bigint,
              executor_id bigint,
              name character varying(255) NOT NULL,
              type event_type_enum NOT NULL DEFAULT \'concert\'::event_type_enum,
              status event_status_enum NOT NULL DEFAULT \'new\'::event_status_enum,
              barcode_type barcode_type_enum NOT NULL DEFAULT \'code128\'::barcode_type_enum,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              date_start timestamp without time zone,
              date_end timestamp without time zone,
              date_entry timestamp without time zone,
              date_sales_start timestamp without time zone,
              date_sales_end timestamp without time zone,
              description text,
              sc_fee real,
              url_alias character varying(512),
              html_caption character varying(512),
              keywords character varying(512),
              meta_description character varying(512),
              system_name character varying(512),
              in_main_slider boolean NOT NULL DEFAULT false,
              in_city_slider boolean NOT NULL DEFAULT false,
              in_main_page boolean NOT NULL DEFAULT false,
              contact text,
              CONSTRAINT event_pkey PRIMARY KEY (id),
              CONSTRAINT event_city_id FOREIGN KEY (city_id)
                  REFERENCES geo_city (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT event_area_file_id FOREIGN KEY (area_file_id)
                  REFERENCES area_file (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT event_creator_id FOREIGN KEY (creator_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT event_exequtor_id FOREIGN KEY (executor_id)
                  REFERENCES executor (id) MATCH SIMPLE
                  ON UPDATE NO ACTION ON DELETE NO ACTION,
              CONSTRAINT event_organizer_id FOREIGN KEY (organizer_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE event OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_event_city_id ON event USING btree (city_id)');
        $this->execute('CREATE INDEX fki_event_area_file_id ON event USING btree (area_file_id)');
        $this->execute('CREATE INDEX fki_event_creator_id ON event USING btree (creator_id)');
        $this->execute('CREATE INDEX fki_event_exequtor_id ON event USING btree (executor_id)');
        $this->execute('CREATE INDEX fki_event_organizer_id ON event USING btree (organizer_id)');
        // post_service
        $this->execute('CREATE TABLE post_service
            (
              id bigserial NOT NULL,
              name character varying(255) NOT NULL,
              description text,
              CONSTRAINT post_service_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE post_service OWNER TO adempiere');
        // post_service_branch
        $this->execute('CREATE TABLE post_service_branch
            (
              id bigserial NOT NULL,
              service_id bigint NOT NULL,
              address_id bigint,
              name character varying(255) NOT NULL,
              "number" character varying(56),
              phone character varying(25),
              CONSTRAINT post_service_branch_pkey PRIMARY KEY (id),
              CONSTRAINT post_service_branch_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT post_service_branch_service_id FOREIGN KEY (service_id)
                  REFERENCES post_service (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE post_service_branch OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_post_service_branch_address_id ON post_service_branch USING btree (address_id)');
        $this->execute('CREATE INDEX fki_post_service_branch_service_id ON post_service_branch USING btree (service_id)');
        // courier
        $this->execute('CREATE TABLE courier
            (
              id bigserial NOT NULL,
              company_id bigint,
              address_id bigint,
              name character varying(255) NOT NULL,
              phone character varying(25),
              CONSTRAINT courier_pkey PRIMARY KEY (id),
              CONSTRAINT courier_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT courier_company_id FOREIGN KEY (company_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE courier OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_courier_address_id ON courier USING btree (address_id)');
        $this->execute('CREATE INDEX fki_courier_company_id ON courier USING btree (company_id)');
        // delivery
        $this->execute('CREATE TABLE delivery
            (
              id bigserial NOT NULL,
              courier_id bigint,
              address_id bigint,
              branch_id bigint,
              sender_id bigint,
              type delivery_type_enum NOT NULL DEFAULT \'pickup\'::delivery_type_enum,
              status delivery_status_enum NOT NULL DEFAULT \'future\'::delivery_status_enum,
              date_planned timestamp without time zone NOT NULL,
              date_sent timestamp without time zone,
              date_done timestamp without time zone,
              receiver_name character varying(255),
              reason text,
              description text,
              post_office_number integer,
              CONSTRAINT delivery_pkey PRIMARY KEY (id),
              CONSTRAINT delivery_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT delivery_branch_id FOREIGN KEY (branch_id)
                  REFERENCES post_service_branch (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT delivery_courier_id FOREIGN KEY (courier_id)
                  REFERENCES courier (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT delivery_sender_id FOREIGN KEY (sender_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE delivery OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_delivery_address_id ON delivery USING btree (address_id)');
        $this->execute('CREATE INDEX fki_delivery_branch_id ON delivery USING btree (branch_id)');
        $this->execute('CREATE INDEX fki_delivery_courier_id ON delivery USING btree (courier_id)');
        $this->execute('CREATE INDEX fki_delivery_sender_id ON delivery USING btree (sender_id)');
        // media
        $this->execute('CREATE TABLE media
            (
              id bigserial NOT NULL,
              entity_id bigint,
              entity_class character varying(56),
              path character varying(512) NOT NULL,
              type media_type_enum NOT NULL DEFAULT \'image\'::media_type_enum,
              status media_status_enum NOT NULL DEFAULT \'main\'::media_status_enum,
              info_json json,
              CONSTRAINT media_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE media OWNER TO adempiere');
        // payment_service
        $this->execute('CREATE TABLE payment_service
            (
              id bigserial NOT NULL,
              name character varying(255) NOT NULL,
              description text,
              CONSTRAINT payment_service_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE payment_service OWNER TO adempiere');
        // payment
        $this->execute('CREATE TABLE payment
            (
              id bigserial NOT NULL,
              recipient_id bigint,
              service_id bigint,
              sum real,
              account character varying(24),
              edrpou character varying(15),
              mfi character varying(8),
              purpose character varying(255),
              name character varying(255),
              description text,
              date_sent timestamp without time zone,
              date_received timestamp without time zone,
              reason text,
              type payment_type_enum NOT NULL DEFAULT \'cash\'::payment_type_enum,
              status payment_status_enum NOT NULL DEFAULT \'not_paid\'::payment_status_enum,
              CONSTRAINT payment_pkey PRIMARY KEY (id),
              CONSTRAINT payment_recipient_id FOREIGN KEY (recipient_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT payment_service_id FOREIGN KEY (service_id)
                  REFERENCES payment_service (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE payment OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_payment_recipient_id ON payment USING btree (recipient_id)');
        $this->execute('CREATE INDEX fki_payment_service_id ON payment USING btree (service_id)');
        // order
        $this->execute('CREATE TABLE "order"
            (
              id bigserial NOT NULL,
              event_id bigint NOT NULL,
              creator_id bigint,
              seller_id bigint,
              delivery_id bigint,
              payment_id bigint,
              client_id bigint,
              partner_id bigint,
              name character varying(255),
              type order_type_enum NOT NULL DEFAULT \'order\'::order_type_enum,
              status order_status_enum NOT NULL DEFAULT \'new\'::order_status_enum,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              date_close timestamp without time zone,
              reason text,
              description text,
              CONSTRAINT order_pkey PRIMARY KEY (id),
              CONSTRAINT order_client_id FOREIGN KEY (client_id)
                  REFERENCES client (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT order_creator_id FOREIGN KEY (creator_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT order_delivery_id FOREIGN KEY (delivery_id)
                  REFERENCES delivery (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT order_event_id FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT order_partner_id FOREIGN KEY (partner_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT order_payment_id FOREIGN KEY (payment_id)
                  REFERENCES payment (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT order_seller_id FOREIGN KEY (seller_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE "order" OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_order_client_id ON "order" USING btree (client_id)');
        $this->execute('CREATE INDEX fki_order_creator_id ON "order" USING btree (creator_id)');
        $this->execute('CREATE INDEX fki_order_delivery_id ON "order" USING btree (delivery_id)');
        $this->execute('CREATE INDEX fki_order_event_id ON "order" USING btree (event_id)');
        $this->execute('CREATE INDEX fki_order_partner_id ON "order" USING btree (partner_id)');
        $this->execute('CREATE INDEX fki_order_seller_id ON "order" USING btree (seller_id)');
        $this->execute('CREATE INDEX fki_order_payment_id ON "order" USING btree (payment_id)');
        // ticket
        $this->execute('CREATE TABLE ticket
            (
              id bigserial NOT NULL,
              event_id bigint NOT NULL,
              zone_id bigint,
              place_id bigint,
              order_id bigint,
              printer_id bigint,
              seller_id bigint,
              price real,
              type ticket_type_enum NOT NULL DEFAULT \'ticket\'::ticket_type_enum,
              status ticket_status_enum NOT NULL DEFAULT \'new\'::ticket_status_enum,
              blank_type ticket_blank_type_enum NOT NULL DEFAULT \'standard\'::ticket_blank_type_enum,
              print_status ticket_print_status_enum NOT NULL DEFAULT \'not_printed\'::ticket_print_status_enum,
              date_print timestamp without time zone,
              date_sold timestamp without time zone,
              barcode character varying(20),
              visitor_id bigint,
              CONSTRAINT ticket_pkey PRIMARY KEY (id),
              CONSTRAINT event_id FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT order_id FOREIGN KEY (order_id)
                  REFERENCES "order" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT ticket_place_id FOREIGN KEY (place_id)
                  REFERENCES area_place (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT ticket_printer_id FOREIGN KEY (printer_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT ticket_seller_id FOREIGN KEY (seller_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT ticket_visitor_id FOREIGN KEY (visitor_id)
                  REFERENCES client (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT ticket_zone_id FOREIGN KEY (zone_id)
                  REFERENCES area_zone (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE ticket OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_event_id ON ticket USING btree (event_id)');
        $this->execute('CREATE INDEX fki_order_id ON ticket USING btree (order_id)');
        $this->execute('CREATE INDEX fki_ticket_place_id ON ticket USING btree (place_id)');
        $this->execute('CREATE INDEX fki_ticket_printer_id ON ticket USING btree (printer_id)');
        $this->execute('CREATE INDEX fki_ticket_seller_id ON ticket USING btree (seller_id)');
        $this->execute('CREATE INDEX fki_ticket_visitor_id ON ticket USING btree (visitor_id)');
        $this->execute('CREATE INDEX fki_ticket_zone_id ON ticket USING btree (zone_id)');
        // quota
        $this->execute('CREATE TABLE quota
            (
              id bigserial NOT NULL,
              event_id bigint NOT NULL,
              partner_id bigint,
              creator_id bigint,
              closer_id bigint,
              name character varying(255),
              type quota_type_enum NOT NULL DEFAULT \'quota\'::quota_type_enum,
              status quota_status_enum NOT NULL DEFAULT \'active\'::quota_status_enum,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              date_close timestamp without time zone,
              partner_fee_type partner_fee_type_enum NOT NULL DEFAULT \'percent\'::partner_fee_type_enum,
              partner_fee real,
              reason text,
              description text,
              CONSTRAINT quota_pkey PRIMARY KEY (id),
              CONSTRAINT quota_closer_id FOREIGN KEY (closer_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT quota_creator_id FOREIGN KEY (creator_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT quota_event_id FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT quota_partner_id FOREIGN KEY (partner_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE quota OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_quota_closer_id ON quota USING btree (closer_id)');
        $this->execute('CREATE INDEX fki_quota_creator_id ON quota USING btree (creator_id)');
        $this->execute('CREATE INDEX fki_quota_event_id ON quota USING btree (event_id)');
        $this->execute('CREATE INDEX fki_quota_partner_id ON quota USING btree (partner_id)');
        // quota_ticket
        $this->execute('CREATE TABLE quota_ticket
            (
              quota_id bigint NOT NULL,
              ticket_id bigint NOT NULL,
              CONSTRAINT quota_ticket_pkey PRIMARY KEY (quota_id, ticket_id),
              CONSTRAINT quota_ticket_quota_id FOREIGN KEY (quota_id)
                  REFERENCES quota (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT quota_ticket_ticket_id FOREIGN KEY (ticket_id)
                  REFERENCES ticket (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE quota_ticket OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_quota_ticket_ticket_id ON quota_ticket USING btree (ticket_id)');
        // client_ticket_request
        $this->execute('CREATE TABLE client_ticket_request
            (
              ticket_id bigint NOT NULL,
              client_id bigint NOT NULL,
              CONSTRAINT client_ticket_request_pkey PRIMARY KEY (ticket_id, client_id),
              CONSTRAINT client_ticket_request_client_id FOREIGN KEY (client_id)
                  REFERENCES client (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT client_ticket_request_ticket_id FOREIGN KEY (ticket_id)
                  REFERENCES ticket (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE client_ticket_request OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_client_ticket_request_client_id ON client_ticket_request USING btree (client_id)');
        // report
        $this->execute('CREATE TABLE report
            (
              id bigserial NOT NULL,
              user_id bigint,
              name character varying(255) NOT NULL,
              path character varying(255) NOT NULL,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              type report_type_enum NOT NULL,
              ext report_ext_enum NOT NULL DEFAULT \'xls\'::report_ext_enum,
              CONSTRAINT report_pkey PRIMARY KEY (id),
              CONSTRAINT report_user_id FOREIGN KEY (user_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE report OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_report_user_id ON report USING btree (user_id)');
        // favorite
        $this->execute('CREATE TABLE favorite
            (
              entity_id bigint NOT NULL,
              entity_class character varying(56) NOT NULL,
              user_id bigint NOT NULL,
              CONSTRAINT favorite_pkey PRIMARY KEY (entity_id, entity_class, user_id),
              CONSTRAINT favorite_user_id FOREIGN KEY (user_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE favorite OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_favorite_user_id ON favorite USING btree (user_id)');
        // story_ticket
        $this->execute('CREATE TABLE story_ticket
            (
              id bigserial NOT NULL,
              ticket_id bigint,
              actor_id bigint,
              action story_ticket_action_enum NOT NULL DEFAULT \'updated\'::story_ticket_action_enum,
              date timestamp without time zone NOT NULL DEFAULT now(),
              event_id bigint NOT NULL,
              zone_id bigint,
              place_id bigint,
              order_id bigint,
              printer_id bigint,
              seller_id bigint,
              price real,
              type ticket_type_enum NOT NULL DEFAULT \'ticket\'::ticket_type_enum,
              status ticket_status_enum NOT NULL DEFAULT \'new\'::ticket_status_enum,
              blank_type ticket_blank_type_enum NOT NULL DEFAULT \'standard\'::ticket_blank_type_enum,
              print_status ticket_print_status_enum NOT NULL DEFAULT \'not_printed\'::ticket_print_status_enum,
              date_print timestamp with time zone,
              date_sold timestamp with time zone,
              barcode character varying(20),
              visitor_id bigint,
              CONSTRAINT story_ticket_pkey PRIMARY KEY (id),
              CONSTRAINT story_ticket_actor_id FOREIGN KEY (actor_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_event_id FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT story_ticket_order_id FOREIGN KEY (order_id)
                  REFERENCES "order" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_place_id FOREIGN KEY (place_id)
                  REFERENCES area_place (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_printer_id FOREIGN KEY (printer_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_seller_id FOREIGN KEY (seller_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_ticket_id FOREIGN KEY (ticket_id)
                  REFERENCES ticket (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_visitor_id FOREIGN KEY (visitor_id)
                  REFERENCES client (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL,
              CONSTRAINT story_ticket_zone_id FOREIGN KEY (zone_id)
                  REFERENCES area_zone (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE story_ticket OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_story_ticket_actor_id ON story_ticket USING btree (actor_id)');
        $this->execute('CREATE INDEX fki_story_ticket_event_id ON story_ticket USING btree (event_id)');
        $this->execute('CREATE INDEX fki_story_ticket_order_id ON story_ticket USING btree (order_id)');
        $this->execute('CREATE INDEX fki_story_ticket_place_id ON story_ticket USING btree (place_id)');
        $this->execute('CREATE INDEX fki_story_ticket_printer_id ON story_ticket USING btree (printer_id)');
        $this->execute('CREATE INDEX fki_story_ticket_seller_id ON story_ticket USING btree (seller_id)');
        $this->execute('CREATE INDEX fki_story_ticket_ticket_id ON story_ticket USING btree (ticket_id)');
        $this->execute('CREATE INDEX fki_story_ticket_visitor_id ON story_ticket USING btree (visitor_id)');
        $this->execute('CREATE INDEX fki_story_ticket_zone_id ON story_ticket USING btree (zone_id)');
        // news
        $this->execute('CREATE TABLE news
            (
              id bigserial NOT NULL,
              name character varying(255) NOT NULL,
              preview text,
              description text,
              url_alias character varying(512),
              html_caption character varying(512),
              meta_description character varying(512),
              keywords character varying(512),
              media_id bigint NOT NULL,
              status news_status_enum NOT NULL DEFAULT \'not_active\'::news_status_enum,
              CONSTRAINT news_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE news OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_news_media_id ON news USING btree (media_id)');
        // slider
        $this->execute('CREATE TABLE slider
            (
              id bigserial NOT NULL,
              event_id bigint NOT NULL,
              big_media_id bigint NOT NULL,
              small_media_id bigint NOT NULL,
              background_color character varying(10) NOT NULL,
              font_color character varying(10) NOT NULL,
              in_main_page boolean NOT NULL DEFAULT true,
              status slider_status_enum NOT NULL DEFAULT \'not_active\'::slider_status_enum,
              CONSTRAINT slider_pkey PRIMARY KEY (id),
              CONSTRAINT slider_event_id_fkey FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE slider OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_slider_event_id ON slider USING btree (event_id)');
        $this->execute('CREATE INDEX fki_slider_big_media_id ON slider USING btree (big_media_id)');
        $this->execute('CREATE INDEX fki_slider_small_media_id ON slider USING btree (small_media_id)');
        // city_slider
        $this->execute('CREATE TABLE city_slider
            (
              geo_city_id bigint NOT NULL,
              slider_id bigint NOT NULL,
              CONSTRAINT city_slider_pkey PRIMARY KEY (geo_city_id, slider_id),
              CONSTRAINT city_slider_geo_city_id_fkey FOREIGN KEY (geo_city_id)
                  REFERENCES geo_city (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT city_slider_slider_id_fkey FOREIGN KEY (slider_id)
                  REFERENCES slider (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE city_slider OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_city_slider_geo_city_id ON city_slider USING btree (geo_city_id)');
        $this->execute('CREATE INDEX fki_city_slider_slider_id ON city_slider USING btree (slider_id)');
        // api_log
        $this->execute('CREATE TABLE api_log
            (
              id bigserial NOT NULL,
              user_id bigint,
              method character varying(6),
              url character varying(2064),
              code integer,
              date timestamp without time zone NOT NULL DEFAULT now(),
              params json,
              response json,
              CONSTRAINT api_log_pkey PRIMARY KEY (id),
              CONSTRAINT api_log_user_id FOREIGN KEY (user_id)
                  REFERENCES "user" (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE SET NULL
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE api_log OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_api_log_user_id ON api_log USING btree (user_id)');
        // cash
        $this->execute('CREATE TABLE cash
            (
              id bigserial NOT NULL,
              company_id bigint NOT NULL,
              address_id bigint NOT NULL,
              name character varying(255),
              status cash_status_enum NOT NULL DEFAULT \'active\'::cash_status_enum,
              date_create timestamp without time zone NOT NULL DEFAULT now(),
              description_json json,
              CONSTRAINT cash_pkey PRIMARY KEY (id),
              CONSTRAINT cash_address_id FOREIGN KEY (address_id)
                  REFERENCES geo_address (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT cash_company_id FOREIGN KEY (company_id)
                  REFERENCES company (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE cash OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_cash_address_id ON cash USING btree (address_id)');
        $this->execute('CREATE INDEX fki_cash_company_id ON cash USING btree (company_id)');
        // upload_barcodes
        $this->execute('CREATE TABLE upload_barcodes
            (
              event_id bigint NOT NULL,
              zone_id bigint,
              place_id bigint,
              barcode character varying(20) NOT NULL,
              CONSTRAINT upload_barcodes_pkey PRIMARY KEY (event_id, barcode),
              CONSTRAINT upload_barcodes_event_id FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT upload_barcodes_place_id FOREIGN KEY (place_id)
                  REFERENCES area_place (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT upload_barcodes_zone_id FOREIGN KEY (zone_id)
                  REFERENCES area_zone (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE upload_barcodes OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_upload_barcodes_event_id ON upload_barcodes USING btree (event_id)');
        $this->execute('CREATE INDEX fki_upload_barcodes_place_id ON upload_barcodes USING btree (place_id)');
        $this->execute('CREATE INDEX fki_upload_barcodes_zone_id ON upload_barcodes USING btree (zone_id)');
        // category
        $this->execute('CREATE TABLE category
            (
              id bigserial NOT NULL,
              key event_category_enum,
              name character varying(255),
              CONSTRAINT category_pkey PRIMARY KEY (id)
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE category OWNER TO adempiere');
        // category_event
        $this->execute('CREATE TABLE category_event
            (
              category_id bigint NOT NULL,
              event_id bigint NOT NULL,
              CONSTRAINT category_event_pkey PRIMARY KEY (category_id, event_id),
              CONSTRAINT category_event_category_id FOREIGN KEY (category_id)
                  REFERENCES category (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT category_event_event_id FOREIGN KEY (event_id)
                  REFERENCES event (id) MATCH SIMPLE
                  ON UPDATE CASCADE ON DELETE CASCADE
            ) WITH (OIDS=FALSE)');
        $this->execute('ALTER TABLE category_event OWNER TO adempiere');
        $this->execute('CREATE INDEX fki_category_event_event_id ON category_event USING btree (event_id)');

        $transaction->commit();
    }

    public function down()
    {
        echo "m160330_131301_install_tables cannot be reverted.\n";

        return false;
    }
}